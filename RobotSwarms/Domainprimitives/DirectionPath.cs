﻿using Microsoft.EntityFrameworkCore;
using RobotSwarms.HICCUP;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.CompilerServices;

namespace RobotSwarms.Domainprimitives
{

    public class DirectionPath
    {

        public DirectionStep? this[int index]
        {
            get
            {
                var query = from step in DirectionSteps
                            where step.Step == index
                            select step;
                return query.FirstOrDefault();
            }
        }

        public List<DirectionStep> DirectionSteps { get; private set; }

        private DirectionPath()
        {
            DirectionSteps = new List<DirectionStep>();
        }

        private DirectionPath(List<DirectionStep> existingPath)
        {
            DirectionSteps = existingPath;
        }



        /// <summary>
        /// Singleton CompassPointPath
        /// </summary>
        [NotMapped]
        public static DirectionPath Empty => new();

        /// <summary>
        /// Path Length
        /// </summary>
        [NotMapped]
        public int Length => DirectionSteps.Count;


        /// <summary>
        /// Adds a new Direction that was moved in
        /// </summary>
        /// <param name="movement">Path with the movement added</param>
        /// <returns></returns>
        public DirectionPath AddMovement(Direction movement)
        {
            var topStep = this[DirectionSteps.Count - 1];
            if (topStep is not null && topStep.Direction == Direction.INVALID) return this;

            if (topStep is null)
                this.DirectionSteps.Add(new(0, movement));
            else
                this.DirectionSteps.Add(new(topStep.Step + 1, movement));

            // Simulating imutability. These sorts of changes dont track well with EF Core
            return this;
        }

        /// <summary>
        /// Finds the inverse of the last movement
        /// </summary>
        /// <returns>direction in which a exploration robot should turn for the "gohome" order</returns>
        /// <exception cref="ExplorationRobotControlException">(or a subclass of this exception) if there is no movement yet</exception>
        public Direction DirectionToGoBackTo()
        {
            var step = this[DirectionSteps.Count - 1];
            if (step is null) throw new ExplorationRobotControlException("Path is empty");
            if (step.Direction == Direction.INVALID) return Direction.INVALID;
            Direction point = step.Direction;
            return point.OppositeDirection();
        }

        /// <summary>
        /// Finds the inverse of the last movement and removes it
        /// </summary>
        /// <returns></returns>
        /// <exception cref="ExplorationRobotControlException"></exception>
        public DirectionPath BacktrackLastMovement()
        {
            var topStep = this[DirectionSteps.Count - 1];
            if (topStep is null) throw new ExplorationRobotControlException("Path is empty");
            if (topStep.Direction == Direction.INVALID) return this;

            this.DirectionSteps.Remove(topStep);
            return this;
        }
    }
}
