﻿using RobotSwarms.HICCUP;

namespace RobotSwarms.Domainprimitives
{
    public class ExplorationRobotOrderParsingException : ExplorationRobotControlException
    {
        public ExplorationRobotOrderParsingException(string message) : base(message)
        {
        }
    }
}
