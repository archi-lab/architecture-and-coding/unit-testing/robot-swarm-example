﻿using RobotSwarms.HICCUP;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RobotSwarms.Domainprimitives
{
    public class Order
    {


        /// <summary>
        /// Types of Orders applicable to ExplorationRobots
        /// </summary>
        public enum OrderType
        {
            SPAWN,
            MINE,
            NORTH,
            EAST,
            SOUTH,
            WEST,
            GOHOME,
            EXPLORE,
            TRANSPORT,
            INVALID
        }

        #region Autoproperties
        [NotMapped]
        public bool IsCompassPointMoveOrder => IsMovement;

        [NotMapped]
        public bool IsSpawnOrder => this.Type == OrderType.SPAWN;

        [NotMapped]
        public bool IsExploreOrder => this.Type == OrderType.EXPLORE;

        [NotMapped]
        public bool IsTransportOrder => this.Type == OrderType.TRANSPORT;

        [NotMapped]
        public bool IsGohomeOrder => this.Type == OrderType.GOHOME;

        [NotMapped]
        public bool IsMineOrder => this.Type == OrderType.MINE;

        #endregion

        public Guid ExplorationRobotId { get; private set;  }
        public OrderType Type { get; private set; }
        public bool IsMovement { get; private set; }


        /// <summary>
        /// The CompassPoint for the move, if isCompassPointMoveOrder() == true. Null otherwise.
        /// </summary>
        public Direction? MoveDirection
        {
            get
            {
                return Type switch
                {
                    OrderType.NORTH => (Direction?)Direction.NORTH,
                    OrderType.SOUTH => (Direction?)Direction.SOUTH,
                    OrderType.WEST => (Direction?)Direction.WEST,
                    OrderType.EAST => (Direction?)Direction.EAST,
                    _ => null,
                };
            }
        }

        #region Constructor

        public Order(Guid orderTarget, OrderType orderType, bool isMovement)
        {

            this.ExplorationRobotId = orderTarget;
            this.Type = orderType;
            this.IsMovement = isMovement;
        }

        public Order() : this(Guid.Empty, OrderType.INVALID, false)
        {
        }
        #endregion


        #region Methods

        /// <summary>
        /// Factory method (instead of constructor)
        /// </summary>
        /// <param name="orderString">the order in string format (see milestone READMEs)</param>
        /// <returns>Order belonging to the order string</returns>
        /// <exception cref="ExplorationRobotControlException">(or a subclass of this exception) if parameter validation fails</exception>
        public static Order FromOrderString(string orderString)
        {
            if (orderString is null)
            {
                throw new ExplorationRobotControlException(nameof(orderString));
            }

            string[] orders = orderString.Split(",");
            string action;
            Guid orderTarget;
            string substring;

            try
            {
                if (orders.Length != 2) throw new ExplorationRobotOrderParsingException("Argument String not in valid format. Expected [<order>,<target>] but got " + orderString);

                if (orders[0][0] == '[') action = orders[0].Substring(1);
                else throw new ExplorationRobotOrderParsingException("Argument String not in valid format. Expected [<order>,<target>] but got " + orderString);

                if (orders[1][^1] == ']') substring = orders[1][..^1];
                else throw new ExplorationRobotOrderParsingException("Argument String not in valid format. Expected [<order>,<target>] but got " + orderString);

            } catch (ExplorationRobotOrderParsingException ex)
            {
                throw new ExplorationRobotControlException("Parsing failed", ex);
            }

            try
            {
                orderTarget = Guid.Parse(substring);
            }
            catch (Exception ex)
            {
                throw new ExplorationRobotControlException("UUID not recognized: " + ex.Message);
            }

            Boolean isMovement;
            OrderType orderType;

            try
            {
                /* NOTE: 
                 * KISS Check too strict for simple switch expressions: This solution stands in conflict with DRY while complexity is unchanged.
                 * However NCSS line count does not properly calculate around switch statements, therefore this solution is necessary to pass CC Tests.
                 */
                isMovement = SwitchNcssCompliantIsMovement(action);
                orderType = SwitchNcssCompliantOrderType(action);
            }
            catch (ExplorationRobotOrderParsingException ex)
            {
                throw new ExplorationRobotControlException("Parsing failed", ex);
            }

            return new Order(orderTarget, orderType, isMovement);
        }

        /// <summary>
        /// Builds an Order from a target and a string
        /// </summary>
        /// <param name="target"></param>
        /// <param name="orderString"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public static Order FromOrderODTString(Guid target, string orderString)
        {
            switch (orderString)
            {
                case "north":
                    return new Order(target, OrderType.NORTH, true);
                case "west":
                    return new Order(target, OrderType.WEST, true);
                case "south":
                    return new Order(target, OrderType.SOUTH, true);
                case "east":
                    return new Order(target, OrderType.EAST, true);
                case "mine":
                    return new Order(target, OrderType.MINE, true);
                default:
                    throw new Exception("Unknown orderstring: '" + orderString + "'");
            }
        }

        /// <summary>
        /// Switches String to OrderType
        /// </summary>
        /// <param name="orderTypeCase"></param>
        /// <returns></returns>
        /// <exception cref="ExplorationRobotOrderParsingException"></exception>
        private static OrderType SwitchNcssCompliantOrderType(String orderTypeCase)
        {
            // More compact than switch-cases -> To pass NCSS Complaince tests
            if (orderTypeCase.Equals("spawn")) return OrderType.SPAWN;
            else if (orderTypeCase.Equals("mine")) return OrderType.MINE;
            else if (orderTypeCase.Equals("north")) return OrderType.NORTH;
            else if (orderTypeCase.Equals("east")) return OrderType.EAST;
            else if (orderTypeCase.Equals("south")) return OrderType.SOUTH;
            else if (orderTypeCase.Equals("west")) return OrderType.WEST;
            else if (orderTypeCase.Equals("gohome")) return OrderType.GOHOME;
            else if (orderTypeCase.Equals("explore")) return OrderType.EXPLORE;
            else if (orderTypeCase.Equals("transport")) return OrderType.TRANSPORT;
            else
                throw new ExplorationRobotOrderParsingException("Argument String does not contain valid order type. Expected either <spawn|mine|north|east|south|west> but got " + orderTypeCase);
        }

        /// <summary>
        /// Determines whether a String is a movement
        /// </summary>
        /// <param name="isMovementCase"></param>
        /// <returns></returns>
        /// <exception cref="ExplorationRobotOrderParsingException"></exception>
        private static bool SwitchNcssCompliantIsMovement(String isMovementCase)
        {
            bool isMovement = false;
            switch (isMovementCase)
            {
                case "spawn":
                    break;
                case "mine":
                    break;
                case "north":
                    isMovement = true;
                    break;
                case "east":
                    isMovement = true;
                    break;
                case "south":
                    isMovement = true;
                    break;
                case "west":
                    isMovement = true;
                    break;
                case "gohome":
                    break;
                case "explore":
                    break;
                case "transport":
                    break;
                default:
                    throw new ExplorationRobotOrderParsingException("Argument String does not contain valid order type. Expected either <spawn|mine|north|east|south|west> but got " + isMovementCase);
            }
            return isMovement;
        }

        #endregion


        #region Autogenerated
        public override bool Equals(object? obj)
        {
            return obj is Order order &&
                   ExplorationRobotId.Equals(order.ExplorationRobotId) &&
                   Type == order.Type &&
                   IsMovement == order.IsMovement;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(ExplorationRobotId, Type, IsMovement);
        }

        public override string? ToString()
        {
            return base.ToString();
        }
        #endregion
    }
}
