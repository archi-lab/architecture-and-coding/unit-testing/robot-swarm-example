﻿using RobotSwarms.HICCUP;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RobotSwarms.Domainprimitives
{

    public class Cargo
    {
        public int Capacity { get; private set; }

        public Ore Ore { get; private set; }


        public Cargo() : this(0)
        { 
        }

        private Cargo(int capacity) : this(capacity, Ore.FromAmount(0))
        {
        }

        private Cargo(int capacity, Ore amount)
        {
            this.Capacity = capacity;
            this.Ore = amount;
        }

        /// <summary>
        /// Factory method (instead of constructor)
        /// </summary>
        /// <param name="capacity">the capacity of the cargo department</param>
        /// <returns> a new, and empty, Cargo</returns>
        /// <exception cref="ExplorationRobotControlException">(or a subclass) if parameter validation fails</exception>
        public static Cargo FromCapacity(int capacity)
        {
            if (capacity < 0) throw new ExplorationRobotControlException("Invalid Argument: Argument cannot be null or negative");
            return new Cargo(capacity);
        }

        /// <summary>
        /// Factory method (instead of constructor)
        /// </summary>
        /// <param name="capacity">the capacity of the cargo department</param>
        /// <param name="ore">the prefilled content in the cargo department</param>
        /// <returns> a new, and empty, Cargo</returns>
        /// <exception cref="ExplorationRobotControlException">(or a subclass) if parameter validation fails</exception>
        public static Cargo FromCapacityAndFilling(int capacity, Ore ore)
        {
            if (ore == null || capacity < 0) throw new ExplorationRobotControlException("Invalid Argument: Argument cannot be null or negative");
            if (capacity < ore.Amount) throw new ExplorationRobotControlException("Invalid Argument State: Orecount exceeds capacity");
            return new Cargo(capacity, ore);
        }

        
        /// <summary>
        /// Finds the amount of ore that could still be taken onboard, until the cargo department is full
        /// </summary>
        /// <returns></returns>
        public Ore AvailableCapacity()
        {
            if (this.Ore.Amount == this.Capacity) return Ore.FromAmount(0);
            return Ore.FromAmount(this.Capacity - this.Ore.Amount);
        }

        /// <summary>
        /// The new cargo after filling the maximum amount possible from a given Ore resource.
        /// </summary>
        /// <example>
        /// If the cargo department has a capacity of 30, is filled with 17, and there is a resource of 22,
        /// then the returned cargo value would be 30. If the resource is only 7, the returned cargo value
        /// would be 24.
        /// </example>
        /// <param name="resource"></param>
        /// <returns></returns>
        /// <exception cref="ExplorationRobotControlException">(or a subclass) if parameter validation fails</exception>
        public Cargo FillFrom(Ore resource)
        {
            if (resource == null || resource.Amount <= 0) throw new ExplorationRobotControlException("Invalid Argument: Argument cannot be null or negative");
            if (resource.IsGreaterThan(this.AvailableCapacity()))
                return new Cargo(this.Capacity, Ore.FromAmount(this.Capacity));
            else
                return new Cargo(this.Capacity, Ore.FromAmount(resource.Amount));
        }


        /// <summary>
        /// The new cargo after filling the maximum amount possible from a given Ore resource.
        /// </summary>
        /// <example>
        /// If the cargo department has a capacity of 30, is filled with 17, and there is a resource of 22,
        /// then the left-behind amount would be 9. If the resource is only 7, the left-behind amount is 0.
        /// </example>
        /// <param name="resource"></param>
        /// <returns></returns>
        /// <exception cref="ExplorationRobotControlException"></exception>
        public Ore LeaveBehindWhenFillingFrom(Ore resource)
        {
            if (resource == null || resource.Amount <= 0) throw new ExplorationRobotControlException("Invalid Argument: Argument cannot be null or negative");
            if (resource.IsGreaterThan(this.AvailableCapacity()))
                return Ore.FromAmount(resource.Amount - this.AvailableCapacity().Amount);
            else
                return Ore.FromAmount(0);
        }

        /// <summary>
        /// Compares two Cargo objects wrt. how full they are
        /// </summary>
        /// <param name="anotherCargo">object to compare to</param>
        /// <returns>true if this object has more free capacity than anotherCargo, false otherwise.</returns>
        /// <exception cref="ExplorationRobotControlException">(or a subclass) if parameter validation fails</exception>
        public bool HasMoreFreeCapacityThan(Cargo anotherCargo)
        {
            if (anotherCargo == null) throw new ExplorationRobotControlException("Invalid Argument: Argument cannot be null or negative");
            return this.Capacity > anotherCargo.Capacity;
        }


        public override string ToString()
        {
            return "Cargo [amount=" + this.Ore + ", capacity=" + this.Capacity + "]";
        }

        public override bool Equals(object? obj)
        {
            return obj is Cargo cargo &&
                   Capacity == cargo.Capacity &&
                   EqualityComparer<Ore>.Default.Equals(Ore, cargo.Ore);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Capacity, Ore);
        }
    }
}
