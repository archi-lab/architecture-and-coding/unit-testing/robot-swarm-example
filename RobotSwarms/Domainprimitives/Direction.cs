﻿namespace RobotSwarms.Domainprimitives
{
    public enum Direction
    {
        NORTH, WEST, SOUTH, EAST, INVALID
    }

    public static class DirectionExtension
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="directionString"></param>
        /// <returns></returns>
        public static Direction FromString(string directionString)
        {
            return directionString switch
            {
                "north" => Direction.NORTH,
                "west" => Direction.WEST,
                "east" => Direction.EAST,
                "south" => Direction.SOUTH,
                _ => throw new ArgumentException("Provided parameter not recognized", nameof(directionString))
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="compassPoint"></param>
        /// <returns></returns>
        /// <exception cref="InvalidOperationException"></exception>
        public static Direction OppositeDirection(this Direction compassPoint)
        {
            return compassPoint switch
            {
                Direction.NORTH => Direction.SOUTH,
                Direction.WEST => Direction.EAST,
                Direction.SOUTH => Direction.NORTH,
                Direction.EAST => Direction.WEST,
                _ => throw new InvalidOperationException("Cannot switch directions for '" + compassPoint + "': Direction not defined")
            };
        }
    }
}
