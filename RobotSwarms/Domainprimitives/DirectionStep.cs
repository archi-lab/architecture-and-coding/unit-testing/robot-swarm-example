﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RobotSwarms.Domainprimitives
{
    public class DirectionStep
    {
        public DirectionStep()
        {
            Step= 0;
            Direction = Direction.INVALID;
        }

        public DirectionStep(int step, Direction direction)
        {
            Step = step;
            Direction = direction;
        }

        public Guid Id { get; set; }
        public int Step { get; init; }
        public Direction Direction { get; init; }
    }
}
