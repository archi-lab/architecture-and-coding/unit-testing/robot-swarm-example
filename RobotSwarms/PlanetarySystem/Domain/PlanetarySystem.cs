﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using RobotSwarms.Config;
using RobotSwarms.Domainprimitives;

namespace RobotSwarms.PlanetarySystem.Domain
{

    public class PlanetarySystem
    {

        public Guid PlanetarySystemId { get; private set; }

        public Ore Ore { get; set; }


        public Guid? NorthPlanetarySystemId { get; set; }
        public virtual PlanetarySystem? NorthPlanetarySystem { get; set; }

        public Guid? SouthPlanetarySystemId { get; set; }
        public virtual PlanetarySystem? SouthPlanetarySystem { get; set; }

        public Guid? EastPlanetarySystemId { get; set; }
        public virtual PlanetarySystem? EastPlanetarySystem { get; set; }

        public Guid? WestPlanetarySystemId { get; set; }
        public virtual PlanetarySystem? WestPlanetarySystem { get; set; }


        public string SystemType { get; set; }

        private static Guid? systemIdentifierUuid;
        private static PlanetarySystem? system = null;


        /// <summary>
        /// Fetches UUID for System where SpaceStation should reside
        /// </summary>
        [NotMapped]
        public static Guid SpaceStationSystemID
        {
            get
            {
                if (systemIdentifierUuid == null)
                {
                    systemIdentifierUuid = Guid.NewGuid();
                }
                return systemIdentifierUuid.Value;
            }
        }

        /// <summary>
        /// Fetches PlanetarySystem where SpaceStation should reside
        /// </summary>
        /// <remarks>Does not Gurantee the PlanetarySystem is saved to its Repository
        /// </remarks>
        [NotMapped]
        public static PlanetarySystem SpaceStationSystem
        {
            get
            {
                if (system == null)
                {
                    string spawnType;
                    try
                    {
                        spawnType = ApplicationConfigurationHelper.GetValueByTag<string>(ApplicationConfigurationHelper.PropertyConstants.SYSTEM_TYPE_SPAWN);

                    }
                    catch (Exception)
                    {
                        spawnType = "space station";
                    }
                    system = new PlanetarySystem(PlanetarySystem.SpaceStationSystemID, 0, spawnType);
                }
                return system;
            }
        }


        public PlanetarySystem(Guid id, int oreCount, string systemType)
        {
            this.PlanetarySystemId = id;
            this.Ore = Ore.FromAmount(oreCount);
            this.SystemType = systemType;
        }


        public PlanetarySystem(Guid id)
        {
            string spawnType;
            try { spawnType = ApplicationConfigurationHelper.GetValueByTag<string>(ApplicationConfigurationHelper.PropertyConstants.SYSTEM_TYPE_UNVISITED); }
            catch (Exception) { spawnType = "regular not visited"; }
            this.PlanetarySystemId = id;
            this.Ore = Domainprimitives.Ore.FromAmount(0);
            this.SystemType = spawnType;

            NorthPlanetarySystem = null;
            SouthPlanetarySystem = null;
            EastPlanetarySystem = null;
            WestPlanetarySystem = null;
        }

        private PlanetarySystem() : this(Guid.Empty) { }
    }
}
