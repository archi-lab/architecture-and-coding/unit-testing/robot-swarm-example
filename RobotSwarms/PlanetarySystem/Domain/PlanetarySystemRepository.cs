﻿using Microsoft.EntityFrameworkCore;
using RobotSwarms.Domainprimitives;
using RobotSwarms.Config;
using RobotSwarms.Shared;

namespace RobotSwarms.PlanetarySystem.Domain
{
    public class PlanetarySystemRepository : ContextProvider<PlanetarySystem>
    {
        public PlanetarySystemRepository() : base() { }

        public DbSet<PlanetarySystem> PlanetarySystems => base.DBSet;

        public PlanetarySystem? FindById(Guid Id)
        {
            return PlanetarySystems.Find(Id);
        }
    }
}
