﻿using RobotSwarms.HICCUP;
using RobotSwarms.Domainprimitives;
using RobotSwarms.PlanetarySystem.Domain;
using RobotSwarms.PlanetarySystem.Errors;

namespace RobotSwarms.PlanetarySystem.Application
{
    public class PlanetarySystemAnalyzingService : IDetect
    {

        public PlanetarySystemRepository PlanetarySystemRepository => new();

        public PlanetarySystemAnalyzingService()
        {
        }

        public void NeighboursDetected(Guid? planetarySystemId, Guid? northNeighbourOrNull, Guid? eastNeighbourOrNull, Guid? southNeighbourOrNull, Guid? westNeighbourOrNull)
        {
            Domain.PlanetarySystem baseSystem;
            using var repository = this.PlanetarySystemRepository;
            
            try
            {
                if (planetarySystemId == null) throw new ArgumentNullException(nameof(planetarySystemId));
                baseSystem = repository.FindById(planetarySystemId.Value)
                    ?? throw new Exception($"Planetary System with ID {planetarySystemId} does not exist.");
            }
            catch (Exception e)
            {
                throw new ExplorationRobotControlException("Neighbours could not be detected: " + e.Message);
            }

            try
            {
                if (northNeighbourOrNull.HasValue)
                {
                    var system = CreateOrGetPlanetarySystem(northNeighbourOrNull.Value);
                    repository.Attach(system);

                    system.SouthPlanetarySystem = baseSystem;
                    baseSystem.NorthPlanetarySystem = system;

                    repository.Update(baseSystem);
                    repository.Update(system);
                    repository.SaveChanges();
                }

                if (eastNeighbourOrNull.HasValue)
                {
                    var system = CreateOrGetPlanetarySystem(eastNeighbourOrNull.Value);
                    repository.Attach(system);

                    system.WestPlanetarySystem = baseSystem;
                    baseSystem.EastPlanetarySystem = system;

                    repository.Update(baseSystem);
                    repository.Update(system);
                    repository.SaveChanges();
                }

                if (southNeighbourOrNull.HasValue)
                {
                    var system = CreateOrGetPlanetarySystem(southNeighbourOrNull.Value);
                    repository.Attach(system);

                    system.NorthPlanetarySystem = baseSystem;
                    baseSystem.SouthPlanetarySystem = system;

                    repository.Update(baseSystem);
                    repository.Update(system);
                    repository.SaveChanges();
                }

                if (westNeighbourOrNull.HasValue)
                {
                    var system = CreateOrGetPlanetarySystem(westNeighbourOrNull.Value);
                    repository.Attach(system);

                    system.EastPlanetarySystem = baseSystem;
                    baseSystem.WestPlanetarySystem = system;

                    repository.Update(baseSystem);
                    repository.Update(system);
                    repository.SaveChanges();
                }
            }
            catch (Exception e)
            {
                throw new ExplorationRobotControlException("Could not create PlanetarySystem.", e);
            }
        }

        private PlanetarySystem.Domain.PlanetarySystem CreateOrGetPlanetarySystem(Guid guid)
        {
            using var repo = new PlanetarySystem.Domain.PlanetarySystemRepository();

            var query = from system in repo.PlanetarySystems
                        where system.PlanetarySystemId == guid
                        select system;

            if (query.Any()) return query.First();

            PlanetarySystem.Domain.PlanetarySystem planetarySystem = new(guid);
            repo.Add(planetarySystem);
            repo.SaveChanges();

            return planetarySystem;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="planetarySystemId"></param>
        /// <param name="ore"></param>
        /// <exception cref="ExplorationRobotControlException"></exception>
        /// <exception cref="ArgumentNullException"></exception>
        public void OreDetected(Guid? planetarySystemId, Ore ore)
        {
            using var repository = this.PlanetarySystemRepository;

            if (ore == null || ore.Amount < 0) throw new ExplorationRobotControlException("oreQuantity cannot be null and has to be greater or equal to 0");
            Domain.PlanetarySystem baseSystem;

            try
            {
                if (planetarySystemId == null) throw new ArgumentNullException(nameof(planetarySystemId));
                baseSystem = repository.FindById(planetarySystemId.Value)
                    ?? throw new Exception($"Planetary System with ID {planetarySystemId} does not exist.");
            }
            catch (Exception e)
            {
                throw new ExplorationRobotControlException("ore in system could not be detected" + e.Message);
            }

            baseSystem.Ore = ore;

            repository.Update(baseSystem);
            repository.SaveChanges();
        }
    }
}
