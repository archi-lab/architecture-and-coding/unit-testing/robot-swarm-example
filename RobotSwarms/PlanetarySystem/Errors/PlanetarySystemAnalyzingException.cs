﻿using RobotSwarms.HICCUP;

namespace RobotSwarms.PlanetarySystem.Errors
{
    public class PlanetarySystemAnalyzingException : ExplorationRobotControlException
    {
        public PlanetarySystemAnalyzingException(string message) : base(message)
        {
        }
    }
}
