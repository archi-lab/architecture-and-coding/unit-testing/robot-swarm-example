﻿using RobotSwarms.Domainprimitives;
using System.Runtime.Intrinsics.X86;
using System;

namespace RobotSwarms.HICCUP
{
    public interface IDetect
    {
        /// <summary>
        /// HICCUP ruft diese Methode in deinem Softwaresystem auf, um dir über neu entdeckte Planetensysteme
        /// Bescheid zu sagen.
        /// </summary>
        /// <param name="planetarySystemId">Die ID des neu entdeckten Planetensystems</param>
        /// <param name="northNeighbourOrNull">
        /// ID des nördlichen Nachbarn, oder null, wenn es keinen nördlichen Nachbarn gibt, 
        /// sondern ein Gebiet mit dunkler Materie</param>
        /// <param name="eastNeighbourOrNull">siehe northNeighbourOrNull</param>
        /// <param name="southNeighbourOrNull">siehe northNeighbourOrNull</param>
        /// <param name="westNeighbourOrNull">siehe northNeighbourOrNull</param>
        public void NeighboursDetected(Guid? planetarySystemId,
            Guid? northNeighbourOrNull, Guid? eastNeighbourOrNull,
            Guid? southNeighbourOrNull, Guid? westNeighbourOrNull);


        /// <summary>
        /// HICCUP ruft diese Methode in deinem Softwaresystem auf, um dir über ein neu entdecktes 
        /// Erzvorkommen Bescheid zu sagen.
        /// </summary>
        /// <param name="planetarySystemId">Die ID Planetensystems, um das es geht</param>
        /// <param name="ore">
        /// Die Menge an entdecktem Erz (kann auch 0 sein, im Fall einer Korrektur, aber nicht <0)
        /// </param>
        void OreDetected(Guid? planetarySystemId, Ore ore);
    }
}
