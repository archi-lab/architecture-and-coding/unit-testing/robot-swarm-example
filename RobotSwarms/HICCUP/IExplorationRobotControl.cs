﻿using RobotSwarms.Domainprimitives;

namespace RobotSwarms.HICCUP
{
    public interface IExplorationRobotControl
    {
        /// <summary>
        /// Führt den gegebenen Befehl an einen Roboter aus.
        /// </summary>
        /// <param name="Order">Befehl an den Roboter (Format siehe README.md)</param>
        /// <exception cref="ExplorationRobotControlException">Falls der Befehl ungültig ist oder nicht ausgeführt werden kann</exception>
        public void ExecuteOrder(Order Order);


        /// <summary>
        /// Gibt zurück, wie viel Erz der Roboter derzeit als Fracht transportiert.
        /// </summary>
        /// <param name="explorationRobotId">Erkundungsroboter, für den die Ladung ermittelt werden soll</param>
        /// <returns>Aktuelle Menge der Erzladung im Explorationsroboter</returns>
        /// <exception cref="ExplorationRobotControlException">Wenn explorationRobotId keine gültige ID ist</exception>
        public Cargo GetExplorationRobotCargo(Guid? explorationRobotId);


        /// <summary>
        /// Gibt zurück, auf welchem Planeten der Roboter gerade steht.
        /// </summary>
        /// <param name="explorationRobotId">ID des Roboters</param>
        /// <returns>ID des Planetensystems, auf dem sich der Erkundungsroboter gerade befindet</returns>
        /// <exception cref="ExplorationRobotControlException">Wenn explorationRobotId keine gültige ID ist</exception>
        public Guid? GetExplorationRobotPlanetarySystem(Guid? explorationRobotId);


        /// <summary>
        /// Gibt den Typ eines Planetensystems zurück.
        /// </summary>
        /// <param name="plantarySystemId">Id des Planetensystems</param>
        /// <returns>
        /// Entweder "space shipyard", "regular visited", oder "regular not visited".
        /// - "space shipyard" heißt: das ist die Raumwerft.
        /// - "regular visited" bedeutet: Das ist nicht die Raumwerft, aber ich kenne das Planetensystem 
        ///   (d.h. seine ID wurde mir von HICCUP schon übermittelt), und mindestens einer meiner Roboter hat
        ///   das Planetensystem schon besucht.
        /// - "regular not visited" heißt: Das ist nicht die Raumwerft, aber ich kenne das Planetensystem 
        ///   (d.h. seine ID wurde mir von HICCUP schon übermittelt), aber keiner meiner Roboter war bis jetzt dort.
        /// </returns>
        /// <exception cref="ExplorationRobotControlException">
        /// Falls die plantarySystemId unbekannt ist, d.h. diese ID wurde von HICCUP noch nicht übermittelt.
        /// </exception>
        public string GetPlanetarySystemType(Guid? plantarySystemId);


        /// <summary>
        /// Gibt die Menge Erz auf einem Planetensystem zurück, soweit bekannt.
        /// </summary>
        /// <param name="planetarySystemId">Id des Planetensystems</param>
        /// <returns>
        /// Die Menge an Titan, die noch auf dem Asteroiden vorhanden ist (kann 0 sein, aber nicht Null)
        /// </returns>
        /// <exception cref="ExplorationRobotControlException">
        /// Falls die plantarySystemId unbekannt ist, d.h. diese ID wurde von HICCUP noch nicht übermittelt.
        /// </exception>
        public Ore GetPlanetarySystemOreAmount(Guid? planetarySystemId);


        /// <summary>
        /// Gibt die Roboter auf einem Planetensystem zurück.
        /// </summary>
        /// <param name="planetarySystemId"></param>
        /// <returns>
        /// Liste der Roboter-IDs, die sich derzeit auf diesem Planetensystem befinden (Liste kann leer 
        /// sein, aber nicht null)
        /// </returns>
        /// <exception cref="ExplorationRobotControlException">
        /// Falls die plantarySystemId unbekannt ist, d.h. diese ID wurde von HICCUP noch nicht übermittelt.
        /// </exception>
        List<Guid> GetPlanetarySystemExplorationRobots(Guid? planetarySystemId);


        /// <summary>
        /// Gibt alle bekannten Planetensysteme zurück.
        /// </summary>
        /// <returns>
        /// Liste aller Planetensysteme, die von HICCUP schon übermittelt wurden (Liste kann leer sein, aber nicht null)
        /// </returns>
        List<Guid> GetKnownPlanetarySystems();


        /// <summary>
        /// Als "Notfallmethode" für das Softwaresystem: setze alles auf den Ausgangszustand zurück. 
        /// Lösche alle Robots und die Galaxiekarte (also alle Planentensysteme). Nur die Raumwerft bleibt übrig, 
        /// mit 0 Erz.
        /// </summary>
        /// <returns>
        /// Die ID des einen übriggebliebenen Planetensystems, auf dem die Raumwerft steht.
        /// </returns>
        public Guid ResetSystem();
    }
}
