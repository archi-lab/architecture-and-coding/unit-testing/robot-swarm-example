﻿namespace RobotSwarms.HICCUP
{
    [Serializable]
    public class ExplorationRobotControlException : Exception
    {
        public ExplorationRobotControlException() { }
        public ExplorationRobotControlException(string message) : base(message) { }
        public ExplorationRobotControlException(string message, Exception inner) : base(message, inner) { }
        protected ExplorationRobotControlException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
}
