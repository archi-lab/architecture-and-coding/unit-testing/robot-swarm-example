﻿
using Microsoft.EntityFrameworkCore;
using RobotSwarms.Config;
using RobotSwarms.Domainprimitives;
using RobotSwarms.Shared;

namespace RobotSwarms.ExplorationRobot.Domain
{
    public class ExplorationRobotRepository : ContextProvider<ExplorationRobot>
    {

        public ExplorationRobotRepository() : base() { }

        public DbSet<ExplorationRobot> ExplorationRobots => base.DBSet;

        public ExplorationRobot? FindById(Guid id)
        {
            return ExplorationRobots
                .Include(e => e.PlanetarySystem)
                .FirstOrDefault(e => e.ExplorationRobotId == id);
        }

        public bool ExistsById(Guid id)
        {
            return ExplorationRobots.Any(e => e.ExplorationRobotId == id);
        }

    }
}
