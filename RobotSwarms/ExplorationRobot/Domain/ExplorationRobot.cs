﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using RobotSwarms.Config;
using RobotSwarms.Domainprimitives;

namespace RobotSwarms.ExplorationRobot.Domain
{
    public class ExplorationRobot
    {

        public Guid ExplorationRobotId { get; private set; }

        public string? Name { get; set; }

        public virtual ICollection<Order> History { get; private set; }


        public Guid? PlanetarySystemId { get; set; }
        public virtual PlanetarySystem.Domain.PlanetarySystem? PlanetarySystem { get; set; }

        public virtual Cargo Cargo
        {
            get; set;
        }

        public virtual DirectionPath DirectionPath
        {
            get; private set;
        }

        private ExplorationRobot() : this(Guid.Empty, null)
        {

        }

        /// <summary>
        /// Default Constructor for ExplorationRobot
        /// </summary>
        /// <param name="id">id ID to be assigned to Robot</param>
        /// <param name="coordinate">coordinate Starting coordinate of ExplorationRobot</param>
        /// <exception cref="InvalidProgramException"></exception>
        public ExplorationRobot(Guid id, PlanetarySystem.Domain.PlanetarySystem? planetarySystem)  : this(id, planetarySystem,0)
        {
        }

        /// <summary>
        /// Default Constructor
        /// </summary>
        /// <param name="id"></param>
        /// <param name="coordinate"></param>
        /// <param name="oreCount"></param>
        public ExplorationRobot(Guid id, PlanetarySystem.Domain.PlanetarySystem? planetarySystem, int oreCount) 
        {
            this.ExplorationRobotId = id;
            this.PlanetarySystem = planetarySystem;
            DirectionPath = DirectionPath.Empty;
            this.Cargo = Cargo.FromCapacityAndFilling(
                ApplicationConfigurationHelper.GetValueByTag<int>(ApplicationConfigurationHelper.PropertyConstants.MAX_ORE),
                Ore.FromAmount(oreCount));

            History = new List<Order>();
        }

        public Ore AddOre(Ore ore)
        {
            var newOre = this.Cargo.LeaveBehindWhenFillingFrom(ore);
            this.Cargo = this.Cargo.FillFrom(ore);
            return newOre;
        }

        public void AddStep(Direction point)
        {
            DirectionPath = DirectionPath.AddMovement(point);
        }

        public Direction GoBack()
        {
            Direction point = DirectionPath.DirectionToGoBackTo();
            DirectionPath = DirectionPath.BacktrackLastMovement();
            return point;
        }

        public void AddToHistory(Order order)
        {
            History.Add(order);
        }

        public ICollection<Order> ResetHistory()
        {
            History = new List<Order>();
            return History;
        }
    }
}
