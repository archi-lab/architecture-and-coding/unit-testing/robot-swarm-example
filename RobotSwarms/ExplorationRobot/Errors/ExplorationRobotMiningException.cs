﻿using RobotSwarms.HICCUP;

namespace RobotSwarms.ExplorationRobot.Errors
{
    public class ExplorationRobotMiningException : ExplorationRobotControlException
    {
        public ExplorationRobotMiningException(string message) : base(message)
        {
        }
    }
}
