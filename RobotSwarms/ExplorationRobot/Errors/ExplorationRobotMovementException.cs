﻿using RobotSwarms.HICCUP;

namespace RobotSwarms.ExplorationRobot.Errors
{
    public class ExplorationRobotMovementException : ExplorationRobotControlException
    {
        public ExplorationRobotMovementException(string message) : base(message)
        {
        }
    }
}
