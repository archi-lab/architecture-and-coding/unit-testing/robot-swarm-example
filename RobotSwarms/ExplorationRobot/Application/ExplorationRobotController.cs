﻿using Microsoft.AspNetCore.Mvc;
using RobotSwarms.HICCUP;
using RobotSwarms.Domainprimitives;
using RobotSwarms.ExplorationRobot.Domain;
using RobotSwarms.PlanetarySystem.Domain;
using Microsoft.EntityFrameworkCore;

namespace RobotSwarms.ExplorationRobot.Application
{
    [ApiController]
    [Route("[controller]")]
    public class ExplorationRobotController : ControllerBase
    {

        [HttpGet("/api/explorationRobots")]
        public IActionResult GetExplorationRobots()
        {
            using var robotsRepo = new ExplorationRobot.Domain.ExplorationRobotRepository();

            var robots = robotsRepo.ExplorationRobots;
            var all = robots.Include(r => r.PlanetarySystem).ToList();

            List<ExplorationRobotDTO> dtos = new();

            foreach (Domain.ExplorationRobot r in all)
            {

                List<OrderDTO> orders = new();
                foreach (Order order in r.History)
                {
                    orders.Add(new OrderDTO(r.ToString(), r.ExplorationRobotId));
                }

                if (r.PlanetarySystem == null) throw new ExplorationRobotControlException();

                ExplorationRobotDTO dto = new ExplorationRobotDTO(r.Name, r.ExplorationRobotId, r.PlanetarySystem.PlanetarySystemId, orders);
                dtos.Add(dto);
            }

            return this.Ok(new JsonResult(dtos.ToArray()));
        }

        [HttpPost("/api/explorationRobots")]
        public IActionResult CreateExplorationRobot([FromBody]ExplorationRobotDTO target)
        {
            using var robotsRepo = new ExplorationRobot.Domain.ExplorationRobotRepository();
            using var planetsRepo = new PlanetarySystem.Domain.PlanetarySystemRepository();

            // target ID and System seem to be empty - if this changes, manually create Robot here
            PlanetarySystem.Domain.PlanetarySystem? startSystem = planetsRepo.FindById(PlanetarySystem.Domain.PlanetarySystem.SpaceStationSystemID);
            robotsRepo.Attach(startSystem);

            ExplorationRobot.Domain.ExplorationRobot newRobot;
            try
            {
                if (startSystem == null) throw new ExplorationRobotControlException("Startsystem is null");
                newRobot = new ExplorationRobot.Domain.ExplorationRobot(Guid.NewGuid(), startSystem);
            }
            catch (Exception e)
            {
                throw new ExplorationRobotControlException("Application incorrectly configured", e);
            }

            newRobot.Name = target.Name;

            robotsRepo.Add(newRobot);
            robotsRepo.SaveChanges();

            if (newRobot.PlanetarySystem == null) throw new ExplorationRobotControlException();

            ExplorationRobotDTO dto = new ExplorationRobotDTO(newRobot.Name, newRobot.ExplorationRobotId, newRobot.PlanetarySystem.PlanetarySystemId, new());

            return this.Created($"/explorationRobots/{newRobot.ExplorationRobotId}", new JsonResult(dto));
        }



        [HttpGet("/api/explorationRobots/{explorationRobot-id}")]
        public IActionResult GetExplorationRobot([FromRoute(Name = "explorationRobot-id")] string rawid)
        {
            using var robotsRepo = new ExplorationRobot.Domain.ExplorationRobotRepository();

            Guid id = Guid.Parse(rawid);

            var query = robotsRepo.FindById(id);
            if (query == null) return this.NotFound();
            ExplorationRobot.Domain.ExplorationRobot r = query;


            List<OrderDTO> orders = new();
            foreach (Order order in r.History)
            {
                orders.Add(new OrderDTO(r.ToString(), r.ExplorationRobotId));
            }

            if (r.PlanetarySystem == null) throw new ExplorationRobotControlException();

            ExplorationRobotDTO dto = new ExplorationRobotDTO(r.Name, r.ExplorationRobotId, r.PlanetarySystem.PlanetarySystemId, orders);

            return this.Ok(new JsonResult(dto));
        }



        [HttpDelete("/api/explorationRobots/{explorationRobot-id}")]
        public IActionResult DeleteExplorationRobot([FromRoute(Name = "explorationRobot-id")] string rawid)
        {
            using var robotsRepo = new ExplorationRobot.Domain.ExplorationRobotRepository();
            Guid id = Guid.Parse(rawid);

            var query = robotsRepo.FindById(id);
            if (query == null) return this.NotFound();

            ExplorationRobot.Domain.ExplorationRobot r = query;


            List<OrderDTO> orders = new();
            foreach (Order order in r.History)
            {
                orders.Add(new OrderDTO(r.ToString(), r.ExplorationRobotId));
            }

            _ = r.PlanetarySystem ?? throw new InvalidOperationException();

            ExplorationRobotDTO dto = new ExplorationRobotDTO(r.Name, null, r.PlanetarySystem.PlanetarySystemId, orders);
            robotsRepo.Remove(r);
            robotsRepo.SaveChanges();
            return this.Ok(new JsonResult(dto));
        }


        [HttpPatch("/api/explorationRobots/{explorationRobot-id}")]
        public IActionResult PatchExplorationRobot([FromRoute(Name = "explorationRobot-id")] string rawid, [FromBody]ExplorationRobotDTO newName)
        {
            using var robotsRepo = new ExplorationRobot.Domain.ExplorationRobotRepository();

            Guid id = Guid.Parse(rawid);
            var query = robotsRepo.FindById(id);
            if (query == null) return this.NotFound();

            ExplorationRobot.Domain.ExplorationRobot r = query;

            r.Name = newName.Name;
            robotsRepo.SaveChanges();

            List<OrderDTO> orders = new();
            foreach (Order order in r.History)
            {
                orders.Add(new OrderDTO(r.ToString(), r.ExplorationRobotId));
            }

            _ = r.PlanetarySystem ?? throw new InvalidOperationException();

            ExplorationRobotDTO dto = new ExplorationRobotDTO(r.Name, r.ExplorationRobotId, r.PlanetarySystem.PlanetarySystemId, orders);

            return this.Ok(new JsonResult(dto));
        }

        [HttpGet("api/reset")]
        public IActionResult Reset() {
            using RobotSwarms.Shared.RobotSwarmsContext robotSwarmsContext = new();

            robotSwarmsContext.Database.EnsureDeleted();
            robotSwarmsContext.SaveChanges();
            robotSwarmsContext.Database.EnsureCreated();
            robotSwarmsContext.SaveChanges();

            var service = new ExplorationRobot.Application.ExplorationRobotControlService();
            service.ResetSystem();

            return this.Ok();
        }
    }
}
