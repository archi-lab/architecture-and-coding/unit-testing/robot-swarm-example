﻿using RobotSwarms.HICCUP;
using RobotSwarms.Config;
using RobotSwarms.Domainprimitives;
using RobotSwarms.ExplorationRobot.Domain;
using RobotSwarms.ExplorationRobot.Errors;
using RobotSwarms.PlanetarySystem.Domain;
using RobotSwarms.WarpGate.Domain;
using System.ComponentModel.DataAnnotations;

namespace RobotSwarms.ExplorationRobot.Application
{
    public class ExplorationRobotControlService : HICCUP.IExplorationRobotControl
    {

        public ExplorationRobotRepository ExplorationRobotRepository => new();
        public PlanetarySystemRepository PlanetarySystemRepository => new();
        public WarpGateRepository WarpGateRepository => new();

        

        public ExplorationRobotControlService()
        {
        }

        public void ExecuteOrder(Order explorationRobotOrder)
        {
            if (explorationRobotOrder == null) throw new ExplorationRobotControlException("Order cannot be null");

            Guid orderTarget = explorationRobotOrder.ExplorationRobotId;

            bool exists = false;

            using (var explorationRobotRepository = ExplorationRobotRepository)
            {
                exists = explorationRobotRepository.ExistsById(orderTarget); //Technically duplicate query, but wont change due to readability -- for now
            }


            if (explorationRobotOrder.IsSpawnOrder)
                Spawn(explorationRobotOrder, exists, orderTarget);
            else
            {
                if (!exists) throw new ExplorationRobotControlException("Could not execute Order: '" + explorationRobotOrder.ToString() + "' : Entity does not exist.");

                try
                {
                    Domain.ExplorationRobot? explorationRobot = null;
                    using (var explorationRobotRepository = ExplorationRobotRepository)
                    {
                        explorationRobot = explorationRobotRepository.FindById(orderTarget);
                    }
                    
                    if (explorationRobot == null) throw new ExplorationRobotControlException("Failed to find ExplorationRobot");



                    if (explorationRobotOrder.IsCompassPointMoveOrder && explorationRobotOrder.MoveDirection is not null)
                        Move(explorationRobotOrder.MoveDirection.Value, explorationRobot);
                    else if (explorationRobotOrder.IsExploreOrder)
                        Explore(explorationRobot);
                    else if (explorationRobotOrder.IsTransportOrder)
                        Transport(explorationRobot);
                    else if (explorationRobotOrder.IsGohomeOrder)
                        Gohome(explorationRobot);
                    else if (explorationRobotOrder.IsMineOrder)
                        Mine(explorationRobot);
                    else
                        throw new InvalidOperationException();


                    // This needs to be called last, as "GoHome" incurs changes that arent tracked and saved by other repositories
                    using (var explorationRobotRepository = ExplorationRobotRepository)
                    {
                        explorationRobot = explorationRobotRepository.FindById(orderTarget) ?? throw new InvalidOperationException();
                        explorationRobot.AddToHistory(explorationRobotOrder);
                        explorationRobotRepository.Update(explorationRobot);
                        explorationRobotRepository.SaveChanges();
                    }
                }
                catch (Exception e)
                {
                    throw new ExplorationRobotControlException("Could not execute Order.", e);
                }
            }

        }

        
        /// <summary>
        /// Attempts to move towards home port
        /// </summary>
        /// <param name="target"></param>
        /// <exception cref="ExplorationRobotControlException"></exception>
        private void Gohome(Domain.ExplorationRobot target)
        {
            using var repo = this.ExplorationRobotRepository;
            repo.Attach(target);
            Direction point = target.GoBack();

            if (point == Direction.INVALID) throw new ExplorationRobotControlException("No way home");
            repo.Update(target);
            repo.SaveChanges();
            Move(point, target, true);
        }

        /// <summary>
        /// Moves target through warpgate
        /// </summary>
        /// <param name="targetRobot"></param>
        /// <exception cref="ExplorationRobotControlException"></exception>
        private void Transport(Domain.ExplorationRobot targetRobot)
        {
            using var explorationRobotRepository = this.ExplorationRobotRepository;
            using var planetarySystemRepository = this.PlanetarySystemRepository;
            using var warpGateRepository = this.WarpGateRepository;

            string systemType;
            try { systemType = ApplicationConfigurationHelper.GetValueByTag<string>(ApplicationConfigurationHelper.PropertyConstants.SYSTEM_TYPE_VISITED); }
            catch (Exception) { systemType = "regular visited"; }

            string systemTypeUnvisited;
            try { systemTypeUnvisited = ApplicationConfigurationHelper.GetValueByTag<string>(ApplicationConfigurationHelper.PropertyConstants.SYSTEM_TYPE_UNVISITED); }
            catch (Exception) { systemTypeUnvisited = "regular not visited"; }

            try
            {
                PlanetarySystem.Domain.PlanetarySystem? currentSystem = targetRobot.PlanetarySystem;
                if (currentSystem == null) throw new ExplorationRobotControlException("Could not find System of target Robot");
                // Fetches WarpGate by SystemId
                WarpGate.Domain.WarpGate gate = warpGateRepository.WarpGates.Single(e => e.Parent == currentSystem);

                PlanetarySystem.Domain.PlanetarySystem? targetSystem = gate.Target;
                if (targetSystem == null) throw new ExplorationRobotControlException("Could not find target System");

                if (targetSystem.SystemType == systemTypeUnvisited) targetSystem.SystemType = systemType;


                targetRobot.AddStep(Direction.INVALID);
                targetRobot.PlanetarySystem = targetSystem;
                planetarySystemRepository.Update(targetSystem);
                explorationRobotRepository.Update(targetRobot);
                planetarySystemRepository.SaveChanges();
                explorationRobotRepository.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new ExplorationRobotControlException("Transport failed: " + ex.Message);
            }
        }


        /// <summary>
        /// Attempts to explore surrounding systems
        /// </summary>
        /// <param name="target"></param>
        /// <exception cref="ExplorationRobotControlException"></exception>
        private void Explore(Domain.ExplorationRobot target)
        {
            using var planetarySystemRepository = PlanetarySystemRepository;


            _ = target.PlanetarySystem;
            List<Direction> directions = new()
            {
                Direction.NORTH,
                Direction.WEST,
                Direction.EAST,
                Direction.SOUTH
            };

            string unvisited;
            try { unvisited = ApplicationConfigurationHelper.GetValueByTag<string>(ApplicationConfigurationHelper.PropertyConstants.SYSTEM_TYPE_UNVISITED); }
            catch (Exception) { unvisited = "regular not visited"; }

            PlanetarySystem.Domain.PlanetarySystem? system = null;
            Direction backupDirection = Direction.INVALID;
            while (directions.Count > 0)
            {
                Direction next = directions[(int)Random.Shared.Next(directions.Count)];
                directions.Remove(next);


                PlanetarySystem.Domain.PlanetarySystem? nextPlanetarySystem = null;
                _ = target.PlanetarySystem ?? throw new InvalidOperationException();

                planetarySystemRepository.Attach(target.PlanetarySystem);
                switch (next)
                {
                    case Direction.NORTH:
                        planetarySystemRepository.Entry(target.PlanetarySystem).Reference(p => p.NorthPlanetarySystem).Load();
                        nextPlanetarySystem = target.PlanetarySystem.NorthPlanetarySystem;
                        break;
                    case Direction.WEST:
                        planetarySystemRepository.Entry(target.PlanetarySystem).Reference(p => p.WestPlanetarySystem).Load();
                        nextPlanetarySystem = target.PlanetarySystem.WestPlanetarySystem;
                        break;
                    case Direction.SOUTH:
                        planetarySystemRepository.Entry(target.PlanetarySystem).Reference(p => p.SouthPlanetarySystem).Load();
                        nextPlanetarySystem = target.PlanetarySystem.SouthPlanetarySystem;
                        break;
                    case Direction.EAST:
                        planetarySystemRepository.Entry(target.PlanetarySystem).Reference(p => p.EastPlanetarySystem).Load();
                        nextPlanetarySystem = target.PlanetarySystem.EastPlanetarySystem;
                        break;
                    case Direction.INVALID:
                        throw new ExplorationRobotMovementException("OrderType not a movement Type");
                    default:
                        throw new ExplorationRobotMovementException("OrderType not a movement Type");
                }

                if (nextPlanetarySystem != null)
                {
                    system = nextPlanetarySystem;
                    if (system.SystemType == unvisited)
                    {
                        Move(next, target);
                        return;
                    }
                    else backupDirection = next;
                }
            }
            if (backupDirection != Direction.INVALID)
            {
                Move(backupDirection, target);
                return;
            }

            throw new ExplorationRobotControlException("Could not find new System to explore");
        }

        /// <summary>
        /// Attempts to spawn Exploration Robot
        /// </summary>
        /// <param name="explorationRobotOrder"></param>
        /// <param name="exists"></param>
        /// <param name="newRobotId"></param>
        /// <exception cref="ExplorationRobotControlException"></exception>
        private void Spawn(Order explorationRobotOrder, bool exists, Guid? newRobotId)
        {
            using var planetarySystemRepository = PlanetarySystemRepository;
            using var explorationRobotRepository = ExplorationRobotRepository;

            if (newRobotId is null)
            {
                throw new ExplorationRobotControlException(nameof(newRobotId));
            }

            if (exists) throw new ExplorationRobotControlException("Could not execute Order: '" + explorationRobotOrder.ToString() + "' : Entity already exists.");

            PlanetarySystem.Domain.PlanetarySystem? system = planetarySystemRepository.FindById(PlanetarySystem.Domain.PlanetarySystem.SpaceStationSystem.PlanetarySystemId);

            if (system == null)
                throw new ExplorationRobotControlException("SpaceStation not in a valid System");

            Domain.ExplorationRobot newRobot;
            try
            {
                newRobot = new Domain.ExplorationRobot(newRobotId.Value, null);
            }
            catch (InvalidProgramException e)
            {
                throw new ExplorationRobotControlException("Application incorrectly configured", e);
            }

            try
            {
                explorationRobotRepository.Add(newRobot);
                explorationRobotRepository.SaveChanges();
                newRobot.PlanetarySystem = system;
                planetarySystemRepository.Update(system);
                planetarySystemRepository.SaveChanges();
                explorationRobotRepository.Update(newRobot);
                explorationRobotRepository.SaveChanges();
            }
            catch (Exception e)
            {
                throw new ExplorationRobotControlException("Failed to spawn robot.", e);
            }
        }

        private void Move(Direction movementOrder, Domain.ExplorationRobot target)
        {
            Move(movementOrder, target, false);
        }

        /// <summary>
        /// Performs a movement action on a JPA ExplorationRobot entity.
        /// </summary>
        /// <param name="movementOrder">The movement-based OrderType for the entity</param>
        /// <param name="target">Target Entity</param>
        /// <param name="suppressLogging">Whether or not the steps are supposed to be logged</param>
        /// <exception cref="ExplorationRobotMovementException"></exception>
        private void Move(Direction movementOrder, Domain.ExplorationRobot target, bool suppressLogging = true)
        {
            using var explorationRobotRepository = ExplorationRobotRepository;
            explorationRobotRepository.Attach(target);

            var current = target.PlanetarySystem;
            if (current == null) throw new ExplorationRobotControlException();

            PlanetarySystem.Domain.PlanetarySystem? nextPlanetarySystem = null;
            explorationRobotRepository.Attach(current);
            switch (movementOrder)
            {
                case Direction.NORTH:
                    explorationRobotRepository.Entry(current).Reference(p => p.NorthPlanetarySystem).Load();
                    nextPlanetarySystem = current.NorthPlanetarySystem;
                    break;
                case Direction.WEST:
                    explorationRobotRepository.Entry(current).Reference(p => p.WestPlanetarySystem).Load();
                    nextPlanetarySystem = current.WestPlanetarySystem;
                    break;
                case Direction.SOUTH:
                    explorationRobotRepository.Entry(current).Reference(p => p.SouthPlanetarySystem).Load();
                    nextPlanetarySystem = current.SouthPlanetarySystem;
                    break;
                case Direction.EAST:
                    explorationRobotRepository.Entry(current).Reference(p => p.EastPlanetarySystem).Load();
                    nextPlanetarySystem = current.EastPlanetarySystem;
                    break;
                case Direction.INVALID:
                    throw new ExplorationRobotMovementException("OrderType not a movement Type");
                default:
                    throw new ExplorationRobotMovementException("OrderType not a movement Type");
            }

            string systemTypeVisited;
            try { systemTypeVisited = (String)ApplicationConfigurationHelper.GetValueByTag<string>(ApplicationConfigurationHelper.PropertyConstants.SYSTEM_TYPE_VISITED); }
            catch (Exception) { systemTypeVisited = "regular visited"; }

            string systemTypeUnvisited;
            try { systemTypeUnvisited = (String)ApplicationConfigurationHelper.GetValueByTag<string>(ApplicationConfigurationHelper.PropertyConstants.SYSTEM_TYPE_UNVISITED); }
            catch (Exception) { systemTypeUnvisited = "regular not visited"; }

            explorationRobotRepository.SaveChanges();
            if (nextPlanetarySystem != null)
            {
                explorationRobotRepository.Attach(nextPlanetarySystem);
                target.PlanetarySystem = nextPlanetarySystem;
                explorationRobotRepository.Update(target);
                explorationRobotRepository.SaveChanges();

                if (nextPlanetarySystem.SystemType == systemTypeUnvisited)
                {
                    nextPlanetarySystem.SystemType = systemTypeVisited;
                }
                explorationRobotRepository.SaveChanges();

                if (!suppressLogging)
                {
                    target.AddStep(movementOrder);
                }

                explorationRobotRepository.SaveChanges();

            }
            else throw new ExplorationRobotMovementException("PlanetarySystem does not exist.");
        }

        /// <summary>
        /// Performs a mining action on a JPA ExplorationRobot entity.
        /// </summary>
        /// <param name="target">Target PlanetarySystem by Guid</param>
        /// <exception cref="ExplorationRobotMiningException"></exception>
        private void Mine(Domain.ExplorationRobot target)
        {
            using var explorationRobotRepository = ExplorationRobotRepository;


            if (!FairnessCheck(target)) throw new ExplorationRobotMiningException("Fairness Check failed");
            PlanetarySystem.Domain.PlanetarySystem? planetarySystem = target.PlanetarySystem ??
                throw new ExplorationRobotMiningException("Failed to find System the Robot is supposed to be in");
            explorationRobotRepository.Attach(planetarySystem);

            Ore ore = planetarySystem.Ore;
            //explorationRobotRepository.Attach(ore);


            if (ore.Amount > 0)
            {
                Ore remainderOre = target.Cargo.LeaveBehindWhenFillingFrom(ore);
                //explorationRobotRepository.Attach(remainderOre);

                //explorationRobotRepository.Attach(target.Cargo);
                target.Cargo = target.Cargo.FillFrom(ore);
                planetarySystem.Ore = remainderOre;

                explorationRobotRepository.Update(planetarySystem);
                explorationRobotRepository.SaveChanges();
                explorationRobotRepository.Update(target);
                explorationRobotRepository.SaveChanges();
            }

            else throw new ExplorationRobotMiningException("No ore present.");
        }

        /// <summary>
        /// Checks for targets with less cargo than the current one, to make sure a mining attempt would be fair
        /// </summary>
        /// <param name="target">the reference target</param>
        /// <returns>whether a current mining attempt would be fair</returns>
        private bool FairnessCheck(Domain.ExplorationRobot target)
        {
            using var explorationRobotRepository = ExplorationRobotRepository;

            var robots = from robot in explorationRobotRepository.ExplorationRobots where robot.PlanetarySystem == target.PlanetarySystem select robot;
            foreach (Domain.ExplorationRobot explorationRobot in robots)
            {
                if (target.Cargo.Ore.IsGreaterThan(explorationRobot.Cargo.Ore)) return false;
            }
            return true;
        }


        public Cargo GetExplorationRobotCargo(Guid? explorationRobotId)
        {
            using var explorationRobotRepository = ExplorationRobotRepository;
            try
            {
                Domain.ExplorationRobot er = explorationRobotRepository.ExplorationRobots.Single(e => e.ExplorationRobotId == explorationRobotId);
                return Cargo.FromCapacityAndFilling(ApplicationConfigurationHelper.GetValueByTag<int>(ApplicationConfigurationHelper.PropertyConstants.MAX_ORE),
                    Ore.FromAmount(er.Cargo.Ore.Amount));
            }
            catch (Exception e)
            {
                throw new ExplorationRobotControlException(e.Message);
            }
        }


        public Guid? GetExplorationRobotPlanetarySystem(Guid? explorationRobotId)
        {
            using var planetarySystemRepository = PlanetarySystemRepository;
            using var explorationRobotRepository = ExplorationRobotRepository;
            if (explorationRobotId is null)
            {
                throw new ExplorationRobotControlException(nameof(explorationRobotId));
            }

            Guid? result = null;
            try
            {
                Domain.ExplorationRobot? explorationRobot = explorationRobotRepository.FindById(explorationRobotId.Value);
                if (explorationRobot == null) throw new ExplorationRobotControlException(nameof(explorationRobot));

                var queryResult = explorationRobot.PlanetarySystem;

                if (queryResult == null)
                    throw new Exception("Illegal State; System does not exist");

                result = queryResult.PlanetarySystemId;


            }
            catch (Exception e)
            {
                throw new ExplorationRobotControlException("Could not find System : " + e.Message);
            }

            return result;
        }


        public string GetPlanetarySystemType(Guid? planetarySystemId)
        {
            using var planetarySystemRepository = PlanetarySystemRepository;
            if (planetarySystemId is null)
            {
                throw new ExplorationRobotControlException(nameof(planetarySystemId));
            }

            try
            {
                PlanetarySystem.Domain.PlanetarySystem? planetarySystem = planetarySystemRepository.FindById(planetarySystemId.Value);
                if (planetarySystem == null) throw new ExplorationRobotControlException(nameof(planetarySystem));
                return planetarySystem.SystemType;
            }
            catch (Exception e)
            {
                throw new ExplorationRobotControlException(e.Message);
            }
        }


        public Ore GetPlanetarySystemOreAmount(Guid? planetarySystemId)
        {
            using var planetarySystemRepository = PlanetarySystemRepository;
            if (planetarySystemId is null)
            {
                throw new ExplorationRobotControlException(nameof(planetarySystemId));
            }

            try
            {
                PlanetarySystem.Domain.PlanetarySystem? planetarySystem = planetarySystemRepository.FindById(planetarySystemId.Value);
                if (planetarySystem == null) throw new ExplorationRobotControlException(nameof(planetarySystem));

                return planetarySystem.Ore;
            }
            catch (Exception e)
            {
                throw new ExplorationRobotControlException(e.Message);
            }
        }


        public List<Guid> GetPlanetarySystemExplorationRobots(Guid? planetarySystemId)
        {
            using var planetarySystemRepository = PlanetarySystemRepository;
            using var explorationRobotRepository = ExplorationRobotRepository;
            if (planetarySystemId is null)
            {
                throw new ExplorationRobotControlException(nameof(planetarySystemId));
            }

            try
            {
                PlanetarySystem.Domain.PlanetarySystem? planetarySystem = planetarySystemRepository.FindById(planetarySystemId.Value);
                if (planetarySystem == null) throw new ExplorationRobotControlException(nameof(planetarySystem));
                var robots =
                    from robot in explorationRobotRepository.ExplorationRobots
                    where robot.PlanetarySystem == planetarySystem
                    select robot;


                List<Guid> robotIDs = new();
                foreach (Domain.ExplorationRobot robot in robots)
                {
                    robotIDs.Add(robot.ExplorationRobotId);
                }
                return robotIDs;
            }
            catch (Exception e)
            {
                throw new ExplorationRobotControlException(e.Message);
            }
        }


        public List<Guid> GetKnownPlanetarySystems()
        {
            using var planetarySystemRepository = PlanetarySystemRepository;
            try
            {
                List<Guid> systems = new();

                foreach (PlanetarySystem.Domain.PlanetarySystem planetarySystem in planetarySystemRepository.PlanetarySystems)
                {
                    systems.Add(planetarySystem.PlanetarySystemId);
                }
                return systems;
            }
            catch (Exception e)
            {
                throw new ExplorationRobotControlException(e.Message);
            }
        }


        public Guid ResetSystem()
        {
            using var planetarySystemRepository = PlanetarySystemRepository;
            using var explorationRobotRepository = ExplorationRobotRepository;

            var robotQuery = from robot in explorationRobotRepository.ExplorationRobots
                             select robot;

            explorationRobotRepository.ExplorationRobots.RemoveRange(robotQuery.ToArray());

            var planetQuery = from planet in planetarySystemRepository.PlanetarySystems
                              select planet;

            planetarySystemRepository.PlanetarySystems.RemoveRange(planetQuery.ToArray());


            PlanetarySystem.Domain.PlanetarySystem spaceStationSystem = PlanetarySystem.Domain.PlanetarySystem.SpaceStationSystem;
            planetarySystemRepository.PlanetarySystems.Add(spaceStationSystem);
            planetarySystemRepository.SaveChanges();
            return spaceStationSystem.PlanetarySystemId;
        }



        public Guid InstallWarpGate(Guid? startPlanetarySystemId, Guid? targetPlanetarySystemId)
        {
            using var planetarySystemRepository = PlanetarySystemRepository;
            using var warpGateRepository = this.WarpGateRepository;
            if (startPlanetarySystemId == null || targetPlanetarySystemId == null)
                throw new ExplorationRobotControlException("Guid cannot be null");

            if (startPlanetarySystemId == targetPlanetarySystemId)
                throw new ExplorationRobotControlException("Target and Start cannot be identical");

            if (warpGateRepository.WarpGates.Any(e => e.Parent.PlanetarySystemId == startPlanetarySystemId))
                throw new ExplorationRobotControlException("Starting System already contains WarpGate");


            try
            {
                PlanetarySystem.Domain.PlanetarySystem? parent = planetarySystemRepository.FindById(startPlanetarySystemId.Value);
                if (parent == null) throw new Exception("Expected PlanetarySystem at Guid " + nameof(startPlanetarySystemId) + " got null instead");

                PlanetarySystem.Domain.PlanetarySystem? target = planetarySystemRepository.FindById(targetPlanetarySystemId.Value);
                if (target == null) throw new Exception("Expected PlanetarySystem at Guid " + nameof(startPlanetarySystemId) + " got null instead");

                WarpGate.Domain.WarpGate gate = new WarpGate.Domain.WarpGate(target, parent);

                warpGateRepository.WarpGates.Add(gate);
                warpGateRepository.SaveChanges();
                return gate.WarpGateId;
            }
            catch (Exception e)
            {
                throw new ExplorationRobotControlException("Could not install warpgate: " + e.Message);
            }
        }



        public void RelocateWarpGate(Guid? warpGateId, Guid? startPlanetarySystemId, Guid? targetPlanetarySystemId)
        {
            using var planetarySystemRepository = PlanetarySystemRepository;
            using var warpGateRepository = this.WarpGateRepository;
            if (!(warpGateId.HasValue && startPlanetarySystemId.HasValue && targetPlanetarySystemId.HasValue))
                throw new ExplorationRobotControlException("Guid cannot be null");

            if (startPlanetarySystemId == targetPlanetarySystemId)
                throw new ExplorationRobotControlException("Target and Start cannot be identical");


            try
            {

                PlanetarySystem.Domain.PlanetarySystem? newParent = planetarySystemRepository.FindById(startPlanetarySystemId.Value);
                if (newParent == null) throw new Exception("Expected PlanetarySystem at Guid " + nameof(startPlanetarySystemId) + " got null instead");

                PlanetarySystem.Domain.PlanetarySystem? newTarget = planetarySystemRepository.FindById(targetPlanetarySystemId.Value);
                if (newTarget == null) throw new Exception("Expected PlanetarySystem at Guid " + nameof(targetPlanetarySystemId) + " got null instead");

                WarpGate.Domain.WarpGate? gate = warpGateRepository.FindById(warpGateId.Value);

                if (gate == null) throw new Exception("No such WarpGate");

                var currentParent = gate.Parent ?? throw new InvalidOperationException();
                bool isIdentical = currentParent.PlanetarySystemId == startPlanetarySystemId;
                bool exists = warpGateRepository.WarpGates.Any(e => e.Parent.PlanetarySystemId == startPlanetarySystemId);

                if (exists && !isIdentical)
                    throw new ExplorationRobotControlException("Starting System already contains WarpGate");

                gate.Parent = newParent;
                gate.Target = newTarget;
                warpGateRepository.Update(gate);
                warpGateRepository.SaveChanges();
            }
            catch (Exception e)
            {
                throw new ExplorationRobotControlException("Could not relocate warpgate: " + e.Message);
            }
        }



        public void ShutdownWarpGate(Guid? warpGateId)
        {
            using var warpGateRepository = this.WarpGateRepository;
            if (warpGateId == null)
                throw new ExplorationRobotControlException("Guid cannot be null");
            try
            {
                WarpGate.Domain.WarpGate gate = warpGateRepository.WarpGates.Single(e => e.WarpGateId == warpGateId);
                warpGateRepository.WarpGates.Remove(gate);
                warpGateRepository.SaveChanges();
            }
            catch (Exception e)
            {
                throw new ExplorationRobotControlException("Could not shut down warpgate: " + e.Message);
            }
        }

        public void ExecuteOrder(string orderString)
        {
            this.ExecuteOrder(Order.FromOrderString(orderString));
        }
    }
}
