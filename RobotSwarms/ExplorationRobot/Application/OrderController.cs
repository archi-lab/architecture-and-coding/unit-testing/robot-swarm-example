﻿using Microsoft.AspNetCore.Mvc;
using RobotSwarms.Domainprimitives;
using RobotSwarms.ExplorationRobot.Domain;
using RobotSwarms.PlanetarySystem.Domain;

namespace RobotSwarms.ExplorationRobot.Application
{
    [ApiController]
    [Route("[controller]")]
    public class OrderController : ControllerBase
    {
        [HttpPost("/api/explorationRobots/{explorationRobot-id}/orders")]
        public IActionResult GiveExplorationRobotOrder([FromRoute(Name = "explorationRobot-id")]string rawid, [FromBody] OrderDTO rawOrder)
        {
            using var robotsRepo = new ExplorationRobot.Domain.ExplorationRobotRepository();
            RobotSwarms.ExplorationRobot.Application.ExplorationRobotControlService robotsService = new();
            if (rawOrder is null || rawOrder.OrderString is null)
            {
                throw new ArgumentNullException(nameof(rawOrder));
            }

            Guid id = Guid.Parse(rawid);
            var query = robotsRepo.FindById(id);
            if (query == null) return this.NotFound();


            ExplorationRobot.Domain.ExplorationRobot r = query;
            Order order;
            try
            {
                order = Order.FromOrderODTString(r.ExplorationRobotId, rawOrder.OrderString);
            }
            catch (Exception)
            {
                return this.BadRequest(rawOrder);
            }
            robotsService.ExecuteOrder(order);

            return this.Created(this.HttpContext.Request.Path,new OrderDTO(r.ToString(), r.ExplorationRobotId));
        }


        [HttpGet("/api/explorationRobots/{explorationRobot-id}/orders")]
        public IActionResult GetExplorationRobotOrderHistory ([FromRoute(Name = "explorationRobot-id")] string rawid)
        {
            using var robotsRepo = new ExplorationRobot.Domain.ExplorationRobotRepository();

            Guid id = Guid.Parse(rawid);
            var query = robotsRepo.FindById(id);
            if (query == null) return this.NotFound();

            ExplorationRobot.Domain.ExplorationRobot r = query;

            ICollection<Order> history = r.History;
            OrderDTO[] orders = new OrderDTO[history.Count];
            int index = 0;
            foreach (Order order in history)
            {
                string orderType = "";
                if (order.IsSpawnOrder) orderType = "spawn";
                else if (order.IsMovement)
                {
                    switch (order.MoveDirection.Value)
                    {
                        case Direction.NORTH:
                            orderType = "north"; break;
                        case Direction.WEST:
                            orderType = "west"; break;
                        case Direction.SOUTH:
                            orderType = "south"; break;
                        case Direction.EAST:
                            orderType = "east"; break;
                    }
                }
                else if (order.IsExploreOrder) orderType = "explore";
                else if (order.IsGohomeOrder) orderType = "gohome";
                else if (order.IsTransportOrder) orderType = "transport";

                orders[index++] = new OrderDTO(orderType, r.ExplorationRobotId);
            }

            return this.Ok(new JsonResult(orders));
        }



        [HttpDelete("/api/explorationRobots/{explorationRobot-id}/orders")]
        public IActionResult ResetExplorationRobotHistory([FromRoute(Name = "explorationRobot-id")] string rawid)
        {
            using var robotsRepo = new ExplorationRobot.Domain.ExplorationRobotRepository();

            Guid id = Guid.Parse(rawid);

            var query = robotsRepo.FindById(id);
            if (query != null)
            {

                ExplorationRobot.Domain.ExplorationRobot r = query;
                var system = r.PlanetarySystem;

                ICollection<Order> history = r.History;
                OrderDTO[] orders = new OrderDTO[history.Count];

                int index = 0;
                foreach (Order order in history)
                {
                    string orderType = "";
                    if (order.IsSpawnOrder) orderType = "spawn";
                    else if (order.IsMovement)
                    {
                        switch (order.MoveDirection.Value)
                        {
                            case Direction.NORTH: 
                                orderType = "north"; break;
                            case Direction.WEST:
                                orderType = "west"; break;
                            case Direction.SOUTH:
                                orderType = "south"; break;
                            case Direction.EAST:
                                orderType = "east"; break;
                        }
                    }
                    else if (order.IsExploreOrder) orderType = "explore";
                    else if (order.IsGohomeOrder) orderType = "gohome";
                    else if (order.IsTransportOrder) orderType = "transport";

                    orders[index++] = new OrderDTO(orderType, r.ExplorationRobotId);
                }

                r.ResetHistory();
                robotsRepo.SaveChanges();

                return this.Ok(new JsonResult(orders));
            }
            return this.NotFound();
        }
    }
}
