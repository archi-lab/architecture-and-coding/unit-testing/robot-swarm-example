﻿using Microsoft.EntityFrameworkCore;
using RobotSwarms.Config;
using RobotSwarms.Domainprimitives;

namespace RobotSwarms.Shared
{
    public class RobotSwarmsContext : SQLServerContext
    {

        protected DbSet<ExplorationRobot.Domain.ExplorationRobot> ExplorationRobots { get; set; }
        protected DbSet<PlanetarySystem.Domain.PlanetarySystem> PlanetarySystems { get; set; }
        protected DbSet<WarpGate.Domain.WarpGate> WarpGates { get; set; }

        public RobotSwarmsContext()
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);



            

            modelBuilder
                .Entity<ExplorationRobot.Domain.ExplorationRobot>()
                .ToTable("ExplorationRobot");

            modelBuilder
                .Entity<ExplorationRobot.Domain.ExplorationRobot>()
                .HasKey(e => e.ExplorationRobotId);




            modelBuilder
                .Entity<ExplorationRobot.Domain.ExplorationRobot>()
                .OwnsOne(e => e.DirectionPath)
                .OwnsMany(e => e.DirectionSteps);

            modelBuilder
                .Owned<Domainprimitives.Cargo>();

            modelBuilder
                .Owned<Domainprimitives.Ore>();

            modelBuilder
                .Entity<ExplorationRobot.Domain.ExplorationRobot>()
                .OwnsOne(e => e.Cargo)
                .OwnsOne(e => e.Ore)
                .WithOwner();

            modelBuilder
                .Entity<ExplorationRobot.Domain.ExplorationRobot>()
                .HasOne(e => e.PlanetarySystem)
                .WithMany()
                .HasForeignKey(e => e.PlanetarySystemId)
                .OnDelete(DeleteBehavior.Restrict);


            modelBuilder
                .Owned<Domainprimitives.Order>();

            modelBuilder
                .Entity<ExplorationRobot.Domain.ExplorationRobot>()
                .OwnsMany(e => e.History)
                .WithOwner()
                .HasForeignKey(o => o.ExplorationRobotId);




            modelBuilder
                .Entity<PlanetarySystem.Domain.PlanetarySystem>()
                .HasKey(p => p.PlanetarySystemId);

            modelBuilder
                .Entity<PlanetarySystem.Domain.PlanetarySystem>()
                .ToTable("PlanetarySystem");

            modelBuilder
                .Entity<PlanetarySystem.Domain.PlanetarySystem>()
                .OwnsOne(p => p.Ore)
                .WithOwner();


            modelBuilder
                .Entity<PlanetarySystem.Domain.PlanetarySystem>()
                .HasOne(p => p.NorthPlanetarySystem)
                .WithMany()
                .HasForeignKey(p => p.NorthPlanetarySystemId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder
                .Entity<PlanetarySystem.Domain.PlanetarySystem>()
                .HasOne(p => p.SouthPlanetarySystem)
                .WithMany()
                .HasForeignKey(p => p.SouthPlanetarySystemId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder
                .Entity<PlanetarySystem.Domain.PlanetarySystem>()
                .HasOne(p => p.WestPlanetarySystem)
                .WithMany()
                .HasForeignKey(p => p.WestPlanetarySystemId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder
                .Entity<PlanetarySystem.Domain.PlanetarySystem>()
                .HasOne(p => p.EastPlanetarySystem)
                .WithMany()
                .HasForeignKey(p => p.EastPlanetarySystemId)
                .OnDelete(DeleteBehavior.Restrict);




            modelBuilder
                .Entity<WarpGate.Domain.WarpGate>()
                .HasKey(w => w.WarpGateId);

            modelBuilder
                .Entity<WarpGate.Domain.WarpGate>()
                .ToTable("WarpGate");

            modelBuilder
                .Entity<WarpGate.Domain.WarpGate>()
                .HasOne(w => w.Parent)
                .WithMany();

            modelBuilder
                .Entity<WarpGate.Domain.WarpGate>()
                .HasOne(w => w.Target)
                .WithMany();
        }

    }
}
