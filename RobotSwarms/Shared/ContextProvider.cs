﻿using Microsoft.EntityFrameworkCore;
using System.Reflection;
using System.Linq;
using Azure.Core;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore.Metadata.Conventions;
using System.Runtime.CompilerServices;

namespace RobotSwarms.Shared
{
    public abstract class ContextProvider<ContextType> : IDisposable where ContextType : class
    {
        private readonly RobotSwarms.Shared.RobotSwarmsContext actualContext;
        private bool disposedValue;

        public DbSet<ContextType> DBSet { 
            get 
            {
                var cType = typeof(RobotSwarms.Shared.RobotSwarmsContext);
                var props = cType.GetProperties(BindingFlags.NonPublic | BindingFlags.Instance);

                var query = from property in typeof(RobotSwarms.Shared.RobotSwarmsContext).GetProperties(BindingFlags.NonPublic | BindingFlags.Instance)
                            where property.PropertyType.IsGenericType
                            && property.PropertyType.GetGenericArguments().Contains(typeof(ContextType))
                            && property.CanRead == true
                            select property;

                if (query.Any())
                {
                    var first = query.First();
                    DbSet<ContextType>? val = first.GetValue(actualContext) as DbSet<ContextType>;
                    if (val != null) return val;
                }

                throw new InvalidOperationException($"ContextType {typeof(ContextType).Name} does not exist within applicationcontext described by {nameof(RobotSwarmsContext)}.");
            } 
        }

        public ContextProvider()
        {
            actualContext = new();
        }

        public EntityEntry Add(object value) => actualContext.Add(value);
        public EntityEntry<TEntity> Add<TEntity>(TEntity value) where TEntity: class => actualContext.Add(value);

        public EntityEntry Update(object value) => actualContext.Update(value);
        public EntityEntry<TEntity> Update<TEntity>(TEntity value) where TEntity : class => actualContext.Update(value);

        public EntityEntry Remove(object value) => actualContext.Remove(value);
        public EntityEntry<TEntity> Remove<TEntity>(TEntity value) where TEntity : class => actualContext.Remove(value);

        public EntityEntry Entry(object value) => actualContext.Entry(value);
        public EntityEntry<TEntity> Entry<TEntity>(TEntity value) where TEntity : class => actualContext.Entry(value);

        public EntityEntry Attach(object value) => actualContext.Attach(value);
        public EntityEntry<TEntity> Attach<TEntity>(TEntity value) where TEntity : class => actualContext.Attach(value);

        public void SaveChanges() => actualContext.SaveChanges();

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    actualContext.Dispose();
                }

                disposedValue = true;
            }
        }

        void IDisposable.Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}
