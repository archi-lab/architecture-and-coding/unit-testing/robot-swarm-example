﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RobotSwarms.WarpGate.Domain
{
    public class WarpGate
    {
        public Guid WarpGateId { get; private set; }

        public PlanetarySystem.Domain.PlanetarySystem? Target { get; set; }

        public PlanetarySystem.Domain.PlanetarySystem? Parent { get; set; }

        public WarpGate(Guid id, PlanetarySystem.Domain.PlanetarySystem? target, PlanetarySystem.Domain.PlanetarySystem? parent)
        {
            WarpGateId = id;
            Target = target;
            Parent = parent;
        }

        public WarpGate(PlanetarySystem.Domain.PlanetarySystem target, PlanetarySystem.Domain.PlanetarySystem parent) : this (Guid.NewGuid(), target, parent)
        {
        }

        private WarpGate() : this (Guid.Empty, null, null) { }
    }
}
