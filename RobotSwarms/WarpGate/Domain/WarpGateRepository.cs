﻿using Microsoft.EntityFrameworkCore;
using RobotSwarms.Config;
using RobotSwarms.Shared;

namespace RobotSwarms.WarpGate.Domain
{
    public class WarpGateRepository : ContextProvider<WarpGate>
    {
        public WarpGateRepository() : base() { }
        public DbSet<WarpGate> WarpGates => base.DBSet;

        public WarpGate? FindById(Guid Id)
        {
            return WarpGates.FirstOrDefault(e => e.WarpGateId == Id);
        }


    }
}
