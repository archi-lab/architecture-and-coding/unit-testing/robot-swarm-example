﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using RobotSwarms.Config;
using RobotSwarms.Domainprimitives;


namespace RobotSwarms.Beacon.Domain
{
    [Index("Identifier", IsUnique = true)]
    [Table("Beacon")]
    public class Beacon
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Required]
        public Guid BeaconId { get; private set; }

        /// <summary>
        /// A unique Identifier for the Beacon. Unlike the ID, this can be changed.
        /// </summary>
        [Required]
        public string Identifier { get; set; }

        /// <summary>
        /// Id of System Beacon belongs to. Null if not assigned
        /// </summary>
        public Guid? SystemId { get; private set;}

        [NotMapped]
        private List<Guid> knownSystems;
        /// <summary>
        /// All (to the Beacon) known System IDs
        /// </summary>
        public IReadOnlyCollection<Guid> KnownSystems { get => knownSystems; }




        public Beacon(Guid beaconId, string identifier)
        {
            knownSystems = new List<Guid>();
            BeaconId = beaconId;
            Identifier = identifier ?? throw new ArgumentNullException(nameof(identifier));
        }

        private Beacon() : this(Guid.Empty, "") { }




        /// <summary>
        /// Adds a new System to the KnownSystems
        /// </summary>
        /// <param name="newSystem">GUID of new known System</param>
        public void AddKnownSystem(Guid newSystem) => knownSystems.Add(newSystem);

        /// <summary>
        /// Resets KnownSystems.
        /// </summary>
        /// <remarks>Does not affect Indentifier, SystemId or BeaconId</remarks>
        public void Reset() => knownSystems.Clear();
    }
}
