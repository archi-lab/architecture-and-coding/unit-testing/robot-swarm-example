﻿using RobotSwarms.Beacon.Domain;
using System.Linq;

namespace RobotSwarms.Beacon.Application
{
    public class BeaconCommunicationService
    {
        public BeaconRepository Beaconrepository { get => new(); }

        public BeaconCommunicationService()
        {
        }

        /// <summary>
        /// Finds and returns the System a Beacon belongs to by using its Indentifier
        /// </summary>
        /// <param name="beaconIdentifier">The String-Identifier representing the beacon</param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public Guid GetBeaconSystemId(string beaconIdentifier)
        {
            using (var repository = new BeaconRepository())
            {
                var query = from beacon in repository.Beacons
                            where beacon.Identifier == beaconIdentifier
                            select beacon;

                if (!query.Any()) throw new Exception($"No Beacon with {nameof(beaconIdentifier)} '{beaconIdentifier}' exists.");
                else
                {

                    Guid? possibleSystem = query.First().SystemId;
                    if (possibleSystem != null) return possibleSystem.Value;
                    else throw new Exception("Beacon is not part of any System");
                }
            }
        }

        /// <summary>
        /// Changes a Beacons Indentifier to a new one and returns the old one
        /// </summary>
        /// <param name="beaconId">ID of beacon to have change applied to</param>
        /// <param name="newBeaconIdentifier">New Identifier</param>
        /// <returns>Old Identifier</returns>
        /// <exception cref="Exception">When Saving Beacon fails or Beacon at ID doesnt exist</exception>
        public string ChangeBeaconIdentifier(Guid beaconId, string newBeaconIdentifier)
        {
            string oldIdentifier = "";
            using (var repository = new BeaconRepository())
            {
                var beacon = repository.FindById(beaconId);

                if (beacon == null) throw new Exception($"Beacon with Id of {beaconId} does not exist");

                oldIdentifier = beacon.Identifier;
                beacon.Identifier = newBeaconIdentifier;

                try
                {
                    //repository.SaveChanges();

                }
                catch (Exception ex)
                {
                    throw new Exception("Saving changes to Beacon failed.", ex);
                }
            }

            return oldIdentifier;
        }
    }
}
