﻿using Microsoft.EntityFrameworkCore;
using RobotSwarms.Domainprimitives;

namespace RobotSwarms.Config
{
    public abstract class SQLServerContext: DbContext
    {
        public SQLServerContext() : base() { }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
            => options.UseSqlServer(@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=RobotSwarms;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            
        }
    }
}
