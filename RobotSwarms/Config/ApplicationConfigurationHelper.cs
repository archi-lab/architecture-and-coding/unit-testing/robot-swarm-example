﻿using System.Text;
using System.Text.Json.Nodes;

namespace RobotSwarms.Config
{
    public class ApplicationConfigurationHelper
    {
        public const string CONFIGURATION_PATH = "/Resources/config.json";
    
        private static bool isLoaded = false;
        private static JsonObject? data;


        public class PropertyConstants
        {
            public const string MAX_ORE = "MAX_ORE";
            public const string SYSTEM_TYPE_VISITED = "SYSTEM_TYPE_VISITED";
            public const string SYSTEM_TYPE_UNVISITED = "SYSTEM_TYPE_UNVISITED";
            public const string SYSTEM_TYPE_SPAWN = "SYSTEM_TYPE_SPAWN";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <exception cref="InvalidProgramException"></exception>
        public static void Load() 
        {
            try 
            {
                string path = Directory.GetCurrentDirectory() + CONFIGURATION_PATH;
                StringBuilder jsonBuilder = new StringBuilder();
                string[] lines = File.ReadAllLines(path);

                foreach (string str in lines)
                {
                    jsonBuilder.AppendLine(str);
                }

                string jsonString = jsonBuilder.ToString();
                var parsedObject = JsonObject.Parse(jsonString);
#pragma warning disable CS8602 // Dereference of a possibly null reference.
                if (parsedObject == null) throw new Exception("Configuration is empty");
                else data = parsedObject.AsObject()["settings"]
                        .AsArray()[0]
                        .AsObject();
#pragma warning restore CS8602 // Dereference of a possibly null reference.

            } catch (Exception e) {
                throw new InvalidProgramException("Configuration file not in readable format or inaccessible" + e.Message);
            }

            isLoaded = true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tag"></param>
        /// <returns></returns>
        /// <exception cref="InvalidOperationException"></exception>
        public static T GetValueByTag<T>(string tag)
        {
            if (!isLoaded) Load();
            try {
                if (data == null) throw new Exception("Data is not loaded");
                var val = data[tag];
                if (val == null) throw new Exception("Cannot return null");
                return val.GetValue<T>();
            } catch (Exception e) {
                throw new InvalidOperationException("Could not load object at " + tag + ": " + e.Message);
            }
        }
    }
}
