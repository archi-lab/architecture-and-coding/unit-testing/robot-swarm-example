﻿using Newtonsoft.Json;
using RobotSwarms.TestHelper.DTO;
using RobotSwarms.TestHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Json;
using System.Text;
using System.Text.Json.Nodes;
using System.Threading.Tasks;

namespace RobotSwarms.Tests.ControllerTests
{
    [TestClass]
    [TestCategory("ControllerTest")]
    public class OrderTests
    {

        [TestMethod("OrderController Test")]
        public async Task TestOrderController()
        {
            using var Setup = RobotSwarms.TestHelper.TestSetup.Seeded;
            using var Server = TestWebHostBuilder.Server;
            using var client = Server.CreateClient();


            RobotSwarms.TestHelper.Generator.StringGenerator stringGenerator = new TestHelper.Generator.StringGenerator(1234);
            ExplorationRobotDTO ERDTO = new ExplorationRobotDTO(stringGenerator.Next(20, 5), null, null);


            #region post api/explorationRobots

            // Act

            var response = await client.PostAsJsonAsync($"{Server.BaseAddress}api/explorationRobots", ERDTO);
            var content = await response.Content.ReadAsStringAsync();
            var location = response.Headers.Location;

            // Assert

            Assert.IsNotNull(response);
            Assert.IsTrue(response.StatusCode == System.Net.HttpStatusCode.Created);
            Assert.IsNotNull(location);

            #endregion


            string rawGuid = location.OriginalString.Split('/')[^1];
            OrderDTO ODTO = new("north", null);


            #region post api/explorationRobots/{explorationRobot-id}/orders

            // Act

            response = await client.PostAsJsonAsync($"{Server.BaseAddress}api{location}/orders", ODTO);

            // Assert

            Assert.IsNotNull(response);
            Assert.IsTrue(response.StatusCode == System.Net.HttpStatusCode.Created);

            #endregion


            #region get api/explorationRobots/{explorationRobot-id}/orders

            // Act

            response = await client.GetAsync($"{Server.BaseAddress}api{location}/orders");
            content = await response.Content.ReadAsStringAsync();
            var val = JsonObject.Parse(content).AsObject()["value"];
            var orders = JsonConvert.DeserializeObject<OrderDTO[]>(val.ToJsonString());

            // Assert

            Assert.IsNotNull(response);
            Assert.IsTrue(response.StatusCode == System.Net.HttpStatusCode.OK);
            Assert.IsNotNull(orders);
            Assert.IsTrue(orders.Length > 0);
            Assert.AreEqual(orders[0].OrderString, ODTO.OrderString);

            #endregion

            #region delete api/explorationRobots/{explorationRobot-id}/orders

            // Act

            response = await client.DeleteAsync($"{Server.BaseAddress}api{location}/orders");

            // Assert

            Assert.IsNotNull(response);
            Assert.IsTrue(response.StatusCode == System.Net.HttpStatusCode.OK);

            #endregion


            #region get api/explorationRobots/{explorationRobot-id}/orders

            // Act

            response = await client.GetAsync($"{Server.BaseAddress}api{location}/orders");
            content = await response.Content.ReadAsStringAsync();
            val = JsonObject.Parse(content).AsObject()["value"];
            orders = JsonConvert.DeserializeObject<OrderDTO[]>(val.ToJsonString());

            // Assert

            Assert.IsNotNull(response);
            Assert.IsTrue(response.StatusCode == System.Net.HttpStatusCode.OK);
            Assert.IsNotNull(orders);
            Assert.IsTrue(orders.Length == 0);

            #endregion
        }
    }
}
