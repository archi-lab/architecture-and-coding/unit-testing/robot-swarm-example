﻿using Newtonsoft.Json;
using Microsoft.AspNetCore.TestHost;
using Microsoft.AspNetCore.Hosting;
using RobotSwarms.TestHelper;
using System.Net.Http.Json;
using System.Text.Json.Nodes;
using Azure;
using Microsoft.AspNetCore.Hosting.Server;
using RobotSwarms.TestHelper.DTO;
using RobotSwarms.TestHelper.DTO;

namespace RobotSwarms.Tests.ControllerTests
{
    [TestClass]
    [TestCategory("ControllerTest")]
    public class ExplorationRobotTests
    {




        [TestMethod("ExplorationRobotController Test")]
        public async Task TestExplorationRobotController()
        {
            using var Setup = RobotSwarms.TestHelper.TestSetup.Empty;
            using var Server = TestWebHostBuilder.Server;
            using var client = Server.CreateClient();

            #region get api/explorationRobots

            // Act

            var response = await client.GetAsync($"{Server.BaseAddress}api/explorationRobots");

            // Assert

            Assert.IsNotNull(response);
            Assert.IsTrue(response.StatusCode == System.Net.HttpStatusCode.OK);

            #endregion

            RobotSwarms.TestHelper.Generator.StringGenerator stringGenerator = new TestHelper.Generator.StringGenerator(1234);
            ExplorationRobotDTO ERDTO = new ExplorationRobotDTO(stringGenerator.Next(20, 5), null, null);

            #region post api/explorationRobots

            // Act

            response = await client.PostAsJsonAsync($"{Server.BaseAddress}api/explorationRobots", ERDTO);
            var content = await response.Content.ReadAsStringAsync();
            var location = response.Headers.Location;

            Guid guid = Guid.Parse(location.OriginalString.Split("/")[^1]);

            // Assert

            Assert.IsNotNull(response);
            Assert.IsTrue(response.StatusCode == System.Net.HttpStatusCode.Created);
            Assert.IsNotNull(location);

            #endregion

            #region get api/explorationRobots/{explorationRobot-id}

            // Act

            response = await client.GetAsync($"{Server.BaseAddress}api{location}");
            content = await response.Content.ReadAsStringAsync();
            var val = JsonObject.Parse(content).AsObject()["value"];
            var robot = JsonConvert.DeserializeObject<ExplorationRobotDTO>(val.ToJsonString());

            // Assert

            Assert.IsNotNull(response);
            Assert.IsTrue(response.StatusCode == System.Net.HttpStatusCode.OK);
            Assert.IsNotNull(robot);
            Assert.IsNotNull(robot.Name);
            Assert.AreEqual(ERDTO.Name, robot.Name);

            #endregion

            #region delete api/explorationRobots/{explorationRobot-id}

            // Act

            response = await client.DeleteAsync($"{Server.BaseAddress}api{location}");

            // Assert

            Assert.IsNotNull(response);
            Assert.IsTrue(response.StatusCode == System.Net.HttpStatusCode.OK);

            #endregion

            #region get api/explorationRobots/{explorationRobot-id}

            // Act

            response = await client.GetAsync($"{Server.BaseAddress}api{location}");

            // Assert

            Assert.IsNotNull(response);
            Assert.IsTrue(response.StatusCode == System.Net.HttpStatusCode.NotFound);

            #endregion

            var ERDTWO = new ExplorationRobotDTO(stringGenerator.Next(20, 5), null, null);

            #region post api/explorationRobots

            // Act

            response = await client.PostAsJsonAsync($"{Server.BaseAddress}api/explorationRobots", ERDTWO);
            content = await response.Content.ReadAsStringAsync();
            location = response.Headers.Location;

            guid = Guid.Parse(location.OriginalString.Split("/")[^1]);


            // Assert

            Assert.IsNotNull(response);
            Assert.IsTrue(response.StatusCode == System.Net.HttpStatusCode.Created);
            Assert.IsNotNull(location);

            #endregion

            #region get api/explorationRobots

            // Act

            response = await client.GetAsync($"{Server.BaseAddress}api/explorationRobots");
            content = await response.Content.ReadAsStringAsync();
            val = JsonObject.Parse(content).AsObject()["value"];
            var robots = JsonConvert.DeserializeObject<ExplorationRobotDTO[]>(val.ToJsonString());

            // Assert

            Assert.IsNotNull(response);
            Assert.IsTrue(response.StatusCode == System.Net.HttpStatusCode.OK);
            Assert.IsNotNull(robots);
            Assert.IsTrue(robots.Length > 0);
            Assert.AreEqual(robots[0].Name, ERDTWO.Name);

            #endregion

            #region patch api/explorationRobots/{explorationRobot-id}

            // Act

            response = await client.PatchAsJsonAsync($"{Server.BaseAddress}api{location}", ERDTO);

            // Assert

            Assert.IsNotNull(response);
            Assert.IsTrue(response.StatusCode == System.Net.HttpStatusCode.OK);

            #endregion

            #region get api/explorationRobots

            // Act

            response = await client.GetAsync($"{Server.BaseAddress}api/explorationRobots");
            content = await response.Content.ReadAsStringAsync();
            val = JsonObject.Parse(content).AsObject()["value"];
            robots = JsonConvert.DeserializeObject<ExplorationRobotDTO[]>(val.ToJsonString());

            // Assert

            Assert.IsNotNull(response);
            Assert.IsTrue(response.StatusCode == System.Net.HttpStatusCode.OK);
            Assert.IsNotNull(robots);
            Assert.IsTrue(robots.Length > 0);
            Assert.AreEqual(robots[0].Name, ERDTO.Name);

            #endregion
        }

        [TestMethod("ExplorationRobotController Test")]
        public async Task TestExplorationRobotControllerNotFound()
        {
            using var Setup = RobotSwarms.TestHelper.TestSetup.Empty;
            using var Server = TestWebHostBuilder.Server;
            using var client = Server.CreateClient();

            #region get api/explorationRobots/{explorationRobot-id}

            // Act

            var response = await client.GetAsync($"{Server.BaseAddress}api/explorationRobots/{Guid.NewGuid()}");
            var content = await response.Content.ReadAsStringAsync();

            // Assert

            Assert.IsNotNull(response);
            Assert.IsTrue(response.StatusCode == System.Net.HttpStatusCode.NotFound);

            #endregion

            RobotSwarms.TestHelper.Generator.StringGenerator stringGenerator = new TestHelper.Generator.StringGenerator(1234);
            ExplorationRobotDTO ERDTO = new ExplorationRobotDTO(stringGenerator.Next(20, 5), null, null);

            #region patch api/explorationRobots/{explorationRobot-id}

            // Act

            response = await client.PatchAsJsonAsync($"{Server.BaseAddress}api/explorationRobots/{Guid.NewGuid()}", ERDTO);
            content = await response.Content.ReadAsStringAsync();

            // Assert

            Assert.IsNotNull(response);
            Assert.IsTrue(response.StatusCode == System.Net.HttpStatusCode.NotFound);

            #endregion

            #region delete api/explorationRobots/{explorationRobot-id}

            // Act

            response = await client.DeleteAsync($"{Server.BaseAddress}api/explorationRobots/{Guid.NewGuid()}");
            content = await response.Content.ReadAsStringAsync();

            // Assert

            Assert.IsNotNull(response);
            Assert.IsTrue(response.StatusCode == System.Net.HttpStatusCode.NotFound);

            #endregion
        }

        public async Task TestExplorationRobotControllerBadRequest()
        {
            using var Setup = RobotSwarms.TestHelper.TestSetup.Empty;
            using var Server = TestWebHostBuilder.Server;
            using var client = Server.CreateClient();


            RobotSwarms.TestHelper.Generator.StringGenerator stringGenerator = new TestHelper.Generator.StringGenerator(1234);
            ExplorationRobotDTO ERDTO = new ExplorationRobotDTO(null, null, null);

            #region post api/explorationRobots

            // Act

            var response = await client.PostAsJsonAsync($"{Server.BaseAddress}api/explorationRobots", ERDTO);
            var content = await response.Content.ReadAsStringAsync();
            var location = response.Headers.Location;

            // Assert

            Assert.IsNotNull(response);
            Assert.IsTrue(response.StatusCode != System.Net.HttpStatusCode.Created);
            Assert.IsNotNull(location);

            #endregion
        }

    }
}
