﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RobotSwarms.Domainprimitives;

namespace RobotSwarms.Tests.DomainPrimitive
{
    [TestClass]
    [TestCategory("DomainPrimitive")]
    public class RuleTests
    {


        [TestMethod("NoSetter")]
        public void TestNoSetter()
        {
            var orderExplorationRobotIdProperty = typeof(Order).GetProperty("ExplorationRobotId");
            Assert.IsNotNull(orderExplorationRobotIdProperty, "Expected Property 'ExplorationRobotId' for Class " + nameof(Order));
            Assert.IsFalse(orderExplorationRobotIdProperty.GetAccessors(false).Length > 1);

            foreach (var property in typeof(Order).GetProperties())
            {
                if (property.DeclaringType == typeof(Direction?) || property.DeclaringType == typeof(Direction))
                {
                    Assert.IsFalse(property.GetAccessors(false).Length > 1);
                }
            }

            var compassPointLengthProperty = typeof(DirectionPath).GetProperty("Length");
            Assert.IsNotNull(compassPointLengthProperty, "Expected Property 'Length' for Class " + nameof(Direction));
            Assert.IsFalse(compassPointLengthProperty.GetAccessors(false).Length > 1);

            var cargoCapacityProperty = typeof(Cargo).GetProperty("Capacity");
            Assert.IsNotNull(cargoCapacityProperty, "Expected Property 'Capacity' for Class " + nameof(Cargo));
            Assert.IsFalse(cargoCapacityProperty.GetAccessors(false).Length > 1);

            var cargoAmountProperty = typeof(Ore).GetProperty("Amount");
            Assert.IsNotNull(cargoAmountProperty, "Expected Property 'Amount' for Class " + nameof(Ore));
            Assert.IsFalse(cargoAmountProperty.GetAccessors(false).Length > 1);
        }
    }
}
