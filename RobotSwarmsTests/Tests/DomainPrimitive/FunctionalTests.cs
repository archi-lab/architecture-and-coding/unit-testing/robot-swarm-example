﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RobotSwarms.Domainprimitives;

namespace RobotSwarms.Tests.DomainPrimitive
{
    [TestClass]
    [TestCategory("DomainPrimitive")]
    public class FunctionalTests
    {

        [TestMethod("CreatedOrderFromString")]
        public void TestCreatedOrderFromString()
        {
            Order compassPointMoveOrder = Order.FromOrderString("[north,137b8ef4-cae4-11ec-9d64-0242ac120002]");
            Order spawnOrder = Order.FromOrderString("[spawn,137b8ef4-cae4-11ec-9d64-0242ac120002]");
            Order mineOrder = Order.FromOrderString("[mine,137b8ef4-cae4-11ec-9d64-0242ac120002]");

            Assert.IsTrue(compassPointMoveOrder.IsCompassPointMoveOrder);
            Assert.IsTrue(spawnOrder.IsSpawnOrder);
            Assert.IsTrue(mineOrder.IsMineOrder);
        }


        [TestMethod("FillCargoWithValidOre")]
        public void TestFillCargoWithValidOre()
        {
            Cargo cargo = Cargo.FromCapacity(42);
            Ore oreA = Ore.FromAmount(21);
            Ore oreB = Ore.FromAmount(69);

            Cargo cargoWithEmptySpace = cargo.FillFrom(oreA);
            Cargo fullCargo = cargo.FillFrom(oreB);

            Assert.AreEqual(21, cargoWithEmptySpace.AvailableCapacity().Amount);
            Assert.AreEqual(0, fullCargo.AvailableCapacity().Amount);

        }

        [TestMethod("LeaveBehindOreWithValidOre")]
        public void TestLeaveBehindOreWithValidOre()
        {
            using var Setup = TestHelper.TestSetup.Empty;
            int capacity = 42;
            int oreAmount = 69;
            int expectedLeftovers = oreAmount - capacity;
            Cargo cargo = Cargo.FromCapacity(42);
            Ore richOre = Ore.FromAmount(69);

            Ore leftoverOre = cargo.LeaveBehindWhenFillingFrom(richOre);

            Assert.AreEqual(expectedLeftovers, leftoverOre.Amount);
        }

        [TestMethod("CargoHasMoreFreeCapacityThanOtherFailure")]
        public void TestCargoHasMoreFreeCapacityThanOtherFailure()
        {
            using var Setup = TestHelper.TestSetup.Empty;
            Ore oreA = Ore.FromAmount(30);
            Ore oreB = Ore.FromAmount(40);
            Cargo cargoA = Cargo.FromCapacity(70);
            Cargo cargoB = Cargo.FromCapacity(50);
            cargoA = cargoA.FillFrom(oreA);
            cargoB = cargoB.FillFrom(oreB);

            Assert.IsTrue(cargoA.HasMoreFreeCapacityThan(cargoB));
        }

        [TestMethod("CompassPointOppositeDirection")]
        public void TestCompassPointOppositeDirection()
        {
            using var Setup = TestHelper.TestSetup.Empty;
            Assert.AreEqual(Direction.SOUTH, Direction.NORTH.OppositeDirection());
            Assert.AreEqual(Direction.NORTH, Direction.SOUTH.OppositeDirection());
            Assert.AreEqual(Direction.WEST, Direction.EAST.OppositeDirection());
            Assert.AreEqual(Direction.EAST, Direction.WEST.OppositeDirection());
        }

        [TestMethod("OreFromAmount")]
        public void TestOreFromAmount()
        {
            using var Setup = TestHelper.TestSetup.Empty;
            int amount = 10;
            Ore ore = Ore.FromAmount(amount);

            Assert.AreEqual(amount, ore.Amount);
        }


        [TestMethod("OreCompare")]
        public void TestOreCompare()
        {
            using var Setup = TestHelper.TestSetup.Empty;
            Ore ore = Ore.FromAmount(10);
            Ore equal = Ore.FromAmount(10);
            Ore lesser = Ore.FromAmount(5);
            Ore greater = Ore.FromAmount(50);
            Ore empty = Ore.FromAmount(0);

            Assert.IsTrue(empty.IsZero);
            Assert.IsTrue(!ore.IsZero);

            Assert.IsTrue(ore.IsGreaterEqualThan(equal));
            Assert.IsTrue(ore.IsGreaterEqualThan(lesser));
            Assert.IsTrue(!ore.IsGreaterEqualThan(greater));

            Assert.IsTrue(ore.IsGreaterThan(lesser));
            Assert.IsTrue(!ore.IsGreaterThan(equal));
            Assert.IsTrue(!ore.IsGreaterThan(greater));
        }

        [TestMethod("OreValueChange")]
        public void TestOreValueChange()
        {
            using var Setup = TestHelper.TestSetup.Empty;
            Ore oreA = Ore.FromAmount(10);
            Ore oreB = Ore.FromAmount(25);

            Assert.AreEqual(35, oreA.AddTo(oreB).Amount);
            Assert.AreEqual(15, oreA.SubtractFrom(oreB).Amount);
            Assert.AreEqual(0, oreB.SubtractFrom(oreA).Amount);
        }

        [TestMethod("CompassPointPathAddAndBacktrack")]
        public void TestCompassPointPathAddAndBacktrack()
        {
            using var Setup = TestHelper.TestSetup.Empty;
            // given
            DirectionPath path = DirectionPath.Empty;
            Assert.AreEqual(0, path.Length);

            // when
            path = path.AddMovement(Direction.NORTH);
            path = path.AddMovement(Direction.WEST);
            path = path.AddMovement(Direction.EAST);
            path = path.AddMovement(Direction.EAST);

            // then
            Assert.AreEqual(4, path.Length);
            Assert.AreEqual(Direction.WEST, path.DirectionToGoBackTo());
            path = path.BacktrackLastMovement();
            Assert.AreEqual(Direction.WEST, path.DirectionToGoBackTo());
            path = path.BacktrackLastMovement();
            Assert.AreEqual(Direction.EAST, path.DirectionToGoBackTo());
            Assert.AreEqual(2, path.Length);
            path = path.BacktrackLastMovement();
            Assert.AreEqual(Direction.SOUTH, path.DirectionToGoBackTo());
        }
    }
}
