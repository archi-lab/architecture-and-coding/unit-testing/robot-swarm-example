﻿using RobotSwarms.Domainprimitives;
using RobotSwarms.HICCUP;

namespace RobotSwarms.Tests.DomainPrimitive
{
    [TestClass]
    [TestCategory("DomainPrimitive")]
    public class ValidationTests
    {
        // Order
        [TestMethod("OrderFailedParsing")]
        public void TestOrderFailedParsingTest()
        {
            Assert.ThrowsException<ExplorationRobotControlException>(() => Order.FromOrderString("[north,4]"));
            Assert.ThrowsException<ExplorationRobotControlException>(() => Order.FromOrderString("[1a553f9a-de70-4587-94b4-82205ae51ff2, west]"));
            Assert.ThrowsException<ExplorationRobotControlException>(() => Order.FromOrderString("[[south,1a553f9a-de70-4587-94b4-82205ae51ff2]]"));
            Assert.ThrowsException<ExplorationRobotControlException>(() => Order.FromOrderString("(east,1a553f9a-de70-4587-94b4-82205ae51ff2)"));

            Assert.ThrowsException<ExplorationRobotControlException>(() => Order.FromOrderString("[spawn,invalidUUID]"));

            Assert.ThrowsException<ExplorationRobotControlException>(() => Order.FromOrderString("[mine,invalidUUID]"));

            // invalid
            Assert.ThrowsException<ExplorationRobotControlException>(() => Order.FromOrderString("[doStuff,42]"));
#pragma warning disable CS0168 // Variable is declared but never used
            Order order;
#pragma warning restore CS0168 // Variable is declared but never used
            Assert.ThrowsException<ExplorationRobotControlException>(() =>
            {
                Order.FromOrderString("[nupf,1a553f9a-de70-4587-94b4-82205ae51ff2]");
            }, "No appropriate validation exception thrown");
            Assert.ThrowsException<ExplorationRobotControlException>(() =>
            {
                Order.FromOrderString("[,1a553f9a-de70-4587-94b4-82205ae51ff2]");
            }, "No appropriate validation exception thrown");
            Assert.ThrowsException<ExplorationRobotControlException>(() =>
            {
                Order.FromOrderString("west,1a553f9a-de70-4587-94b4-82205ae51ff2]");
            }, "No appropriate validation exception thrown");

        }


#pragma warning disable CS8625 // Cannot convert null literal to non-nullable reference type.
        [TestMethod("CargoCreatedFailing")]
        public void TestCargoCreatedFailingTest()
        {
            //Assert.ThrowsException<ExplorationRobotControlException>(() => Cargo.FromCapacity(null),"No null check exception!");
            Assert.ThrowsException<ExplorationRobotControlException>(() => Cargo.FromCapacity(-1),
                            "No validation exception for negative capacity!");
            Assert.ThrowsException<ExplorationRobotControlException>(
                            () => Cargo.FromCapacityAndFilling(30, null),
                            "No validation exception for null filling!");
            Assert.ThrowsException<ExplorationRobotControlException>(
                            () => Cargo.FromCapacityAndFilling(10, Ore.FromAmount(15)),
                            "No validation exception for filling > capacity!");
        }

        [TestMethod("FillCargoWithInvalidOre")]
        public void TestFillCargoWithInvalidOre()
        {
            Cargo cargo = Cargo.FromCapacity(42);
            Ore ore = Ore.FromAmount(0);
            Assert.ThrowsException<ExplorationRobotControlException>(() => cargo.FillFrom(null));
            Assert.ThrowsException<ExplorationRobotControlException>(() => cargo.FillFrom(ore));
        }

        [TestMethod("leaveBehindResourceWithInvalidOre")]
        public void TestLeaveBehindResourceWithInvalidOre()
        {
            Cargo cargo = Cargo.FromCapacity(42);
            Ore emptyOre = Ore.FromAmount(0);

            Assert.ThrowsException<ExplorationRobotControlException>(() => cargo.LeaveBehindWhenFillingFrom(null));
            Assert.ThrowsException<ExplorationRobotControlException>(() => cargo.LeaveBehindWhenFillingFrom(emptyOre));
        }

        [TestMethod("CargoHasMoreFreeCapacityThanOtherFailure")]
        public void TestCargoHasMoreFreeCapacityThanOtherFailure()
        {
            Cargo cargo = Cargo.FromCapacity(42);
            Assert.ThrowsException<ExplorationRobotControlException>(() => cargo.HasMoreFreeCapacityThan(null));
        }

        // Ore
        [TestMethod("OreFromAmountFailure")]
        public void TestOreFromAmountFailure()
        {
            // This doesnt work anyways Assert.ThrowsException<ExplorationRobotControlException>(() => Ore.FromAmount(null));
            Assert.ThrowsException<ExplorationRobotControlException>(() => Ore.FromAmount(-1));
        }

        [TestMethod("OreCompareFailure")]
        public void TestOreCompareFailure()
        {
            Ore ore = Ore.FromAmount(10);

            Assert.ThrowsException<ExplorationRobotControlException>(() => ore.IsGreaterThan(null));
            Assert.ThrowsException<ExplorationRobotControlException>(() => ore.IsGreaterEqualThan(null));
        }

        [TestMethod("OreValueChangeFailure")]
        public void TestOreValueChangeFailure()
        {
            Ore ore = Ore.FromAmount(10);

            Assert.ThrowsException<ExplorationRobotControlException>(() => ore.AddTo(null));
            Assert.ThrowsException<ExplorationRobotControlException>(() => ore.SubtractFrom(null));
        }
#pragma warning restore CS8625 // Cannot convert null literal to non-nullable reference type.



        // CompassPointPath
        [TestMethod("NoActionsForEmptyCompassPointPath")]
        public void TestNoActionsForEmptyCompassPointPath()
        {
            Assert.ThrowsException<ExplorationRobotControlException>(() =>
            {
                DirectionPath.Empty.DirectionToGoBackTo();
            });
            Assert.ThrowsException<ExplorationRobotControlException>((Action)(() =>
            {
                DirectionPath.Empty.BacktrackLastMovement();
            }));
        }
    }
}
