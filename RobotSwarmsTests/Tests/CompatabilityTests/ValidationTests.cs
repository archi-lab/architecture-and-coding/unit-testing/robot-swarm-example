﻿using RobotSwarms.Domainprimitives;
using RobotSwarms.HICCUP;

namespace RobotSwarms.Tests.CompatabilityTests
{
    [TestClass]
    [TestCategory("CompatabilityTests")]
    public class ValidationTests
    {

        [TestMethod("ExecuteOrderValidation")]
        public void TestExecuteOrderValidation()
        {

            using var Setup = TestHelper.TestSetup.Empty;

            Assert.ThrowsException<ExplorationRobotControlException>(() =>
            {
                Setup.ExplorationRobotControl.ExecuteOrder(null);
            }, "Validation error expected for invalid order <null>");

            string s1 = "";
            Assert.ThrowsException<ExplorationRobotControlException>(() =>
            {
                Order order = Order.FromOrderString(s1);
                Setup.ExplorationRobotControl.ExecuteOrder(order);
            }, "Validation error expected for invalid order " + s1);

            string s2 = "[eeast,554c00de-3a81-43e8-a384-2dcfebdcf34c]";
            Assert.ThrowsException<ExplorationRobotControlException>(() =>
            {
                Order order = Order.FromOrderString(s2);
                Setup.ExplorationRobotControl.ExecuteOrder(order);
            }, "Validation error expected for invalid order " + s2);

            string s3 = "[,554c00de-3a81-43e8-a384-2dcfebdcf34c]";
            Assert.ThrowsException<ExplorationRobotControlException>(() =>
            {
                Order order = Order.FromOrderString(s3);
                Setup.ExplorationRobotControl.ExecuteOrder(order);
            }, "Validation error expected for invalid order " + s3);

            string s4 = "[gohome,554c00de-3a81-43e8-a384-2dcfebdcf34c";
            Assert.ThrowsException<ExplorationRobotControlException>(() =>
            {
                Order order = Order.FromOrderString(s4);
                Setup.ExplorationRobotControl.ExecuteOrder(order);
            }, "Validation error expected for invalid order " + s4);

            string s5 = "[explore,000000-000-00]";
            Assert.ThrowsException<ExplorationRobotControlException>(() =>
            {
                Order order = Order.FromOrderString(s5);
                Setup.ExplorationRobotControl.ExecuteOrder(order);
            }, "Validation error expected for invalid order " + s5);
        }

        [TestMethod("GetExplorationRobotCargo")]
        public void TestGetExplorationRobotCargo()
        {
            using var Setup = TestHelper.TestSetup.Empty;

            Assert.ThrowsException<ExplorationRobotControlException>(() =>
            {
                Setup.ExplorationRobotControl.GetExplorationRobotCargo(null);
            }, "Validation error expected for invalid UUID <null>");
        }

        [TestMethod("GetExplorationRobotPlanetarySystem")]
        public void TestGetExplorationRobotPlanetarySystem()
        {
            using var Setup = TestHelper.TestSetup.Empty;

            Assert.ThrowsException<ExplorationRobotControlException>(() =>
            {
                Setup.ExplorationRobotControl.GetExplorationRobotPlanetarySystem(null);
            }, "Validation error expected for invalid UUID <null>");
        }

        [TestMethod("GetPlanetarySystemType")]
        public void TestGetPlanetarySystemType()
        {
            using var Setup = TestHelper.TestSetup.Empty;

            Assert.ThrowsException<ExplorationRobotControlException>(() =>
            {
                Setup.ExplorationRobotControl.GetPlanetarySystemType(null);
            }, "Validation error expected for invalid UUID <null>");
        }

        [TestMethod("GetPlanetarySystemOreAmount")]
        public void TestGetPlanetarySystemOreAmount()
        {
            using var Setup = TestHelper.TestSetup.Empty;

            Assert.ThrowsException<ExplorationRobotControlException>(() =>
            {
                Setup.ExplorationRobotControl.GetPlanetarySystemOreAmount(null);
            }, "Validation error expected for invalid UUID <null>");
        }

        [TestMethod("GetPlanetarySystemExplorationRobots")]
        public void TestGetPlanetarySystemExplorationRobots()
        {
            using var Setup = TestHelper.TestSetup.Empty;

            Assert.ThrowsException<ExplorationRobotControlException>(() =>
            {
                Setup.ExplorationRobotControl.GetPlanetarySystemExplorationRobots(null);
            }, "Validation error expected for invalid UUID <null>");
        }

        [TestMethod("NeighboursDetected")]
        public void TestNeighboursDetected()
        {
            using var Setup = TestHelper.TestSetup.Empty;

            Assert.ThrowsException<ExplorationRobotControlException>(() =>
            {
                Setup.Detect.NeighboursDetected(null, null, null, null, null);
            }, "Validation error expected for invalid UUID <null>");
        }

        [TestMethod("OreDetected")]
        public void TestOreDetected()
        {
            using var Setup = TestHelper.TestSetup.Empty;

            Guid northNeighbor = Guid.NewGuid();

            Setup.Detect.NeighboursDetected(Setup.StartSystem, northNeighbor, null, null, null);
            Setup.Detect.OreDetected(northNeighbor, Ore.FromAmount(5));

            // then
            Assert.ThrowsException<ExplorationRobotControlException>(() =>
            {
                Setup.Detect.OreDetected(null, Ore.FromAmount(1));
            }, "Validation error expected for invalid UUID <null>");

            Assert.ThrowsException<ExplorationRobotControlException>(() =>
            {
                Setup.Detect.OreDetected(northNeighbor, null);
            }, "Validation error expected for invalid quantity <null>");

            Assert.ThrowsException<ExplorationRobotControlException>(() =>
            {
                Ore ore = Ore.FromAmount(-3);
                Setup.Detect.OreDetected(northNeighbor, ore);
            }, "Validation error expected for invalid quantity -3");
        }
    }
}
