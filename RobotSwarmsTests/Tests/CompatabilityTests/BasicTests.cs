﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RobotSwarms.Domainprimitives;
using RobotSwarms.HICCUP;

namespace RobotSwarms.Tests.CompatabilityTests
{
    [TestClass]
    [TestCategory("CompatabilityTests")]
    public class BasicTests
    {


        [TestMethod("JustOnePlanetarySystemFoundAfterReset")]
        public void TestJustOnePlanetarySystemFoundAfterReset()
        {
            using var Setup = TestHelper.TestSetup.Empty;

            var systems = Setup.ExplorationRobotControl.GetKnownPlanetarySystems();
            Assert.AreEqual(1, systems.Count);
            string stationType = Setup.ExplorationRobotControl.GetPlanetarySystemType(systems[0]);
            Assert.AreEqual("space station", stationType);
        }



        [TestMethod("NoMiningOnZero")]
        public void TestNoMiningOnZero()
        {
            using var Setup = TestHelper.TestSetup.Empty;

            Guid guid = Guid.NewGuid();
            var spawnOrder = ("[spawn, " + guid + "]");
            var mineOrder = ("[mine," + guid + "]");

            Setup.ExplorationRobotControl.ExecuteOrder(Order.FromOrderString(spawnOrder));

            Assert.ThrowsException<ExplorationRobotControlException>(() => Setup.ExplorationRobotControl.ExecuteOrder(Order.FromOrderString(mineOrder)));
        }



        [TestMethod("NoDoubleSpawn")]
        public void TestNoDoubleSpawn()
        {
            using var Setup = TestHelper.TestSetup.Empty;

            Guid guid = Guid.NewGuid();
            var spawnOrder = ("[spawn, " + guid + "]");

            Setup.ExplorationRobotControl.ExecuteOrder(Order.FromOrderString(spawnOrder));

            Assert.ThrowsException<ExplorationRobotControlException>(() => Setup.ExplorationRobotControl.ExecuteOrder(Order.FromOrderString(spawnOrder)));
        }



        [TestMethod("NoDoubleMine")]
        public void TestNoDoubleMine()
        {
            using var Setup = TestHelper.TestSetup.Empty;

            Guid northNeighbor = Guid.NewGuid();

            Setup.Detect.NeighboursDetected(Setup.StartSystem, northNeighbor, null, null, null);
            Setup.Detect.OreDetected(northNeighbor, Ore.FromAmount(5));

            Guid robotId = Guid.NewGuid();

            object[] orders = new object[]
            {
                "[spawn," + robotId + "]",
                "[north," + robotId + "]",
                "[mine," + robotId + "]"
            };

            foreach (string order in orders)
                Setup.ExplorationRobotControl.ExecuteOrder(Order.FromOrderString(order));

            Assert.ThrowsException<ExplorationRobotControlException>(() => Setup.ExplorationRobotControl.ExecuteOrder(Order.FromOrderString($"{orders[^1]}")));
        }
    }
}
