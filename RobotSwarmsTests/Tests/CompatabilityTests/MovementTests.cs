﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RobotSwarms.Domainprimitives;
using RobotSwarms.HICCUP;

namespace RobotSwarms.Tests.CompatabilityTests
{
    [TestClass]
    [TestCategory("CompatabilityTests")]
    public class MovementTests
    {

        [TestMethod("PlanetarySystemTypeOk")]
        public void TestPlanetarySystemTypeOk()
        {
            using var Setup = TestHelper.TestSetup.Seeded;
            Guid robotId = Guid.NewGuid();

            object[] testData = new object[]
            {
                ("[spawn," + robotId + "]"),
                ("[north," + robotId + "]")
            };

            foreach (string order in testData)
                Setup.ExplorationRobotControl.ExecuteOrder(Order.FromOrderString(order));

            Assert.AreEqual("regular visited",
                Setup.ExplorationRobotControl.GetPlanetarySystemType(
                    Setup.ExplorationRobotControl.GetExplorationRobotPlanetarySystem(robotId).Value));

            Assert.AreEqual("regular not visited", Setup.ExplorationRobotControl.GetPlanetarySystemType(Setup.Systems.ElementAt(6).Value));
            Assert.AreEqual("regular not visited", Setup.ExplorationRobotControl.GetPlanetarySystemType(Setup.Systems.ElementAt(11).Value));
        }

        [TestMethod("MoveAgainstBlock")]
        public void TestMoveAgainstBlock()
        {
            using var Setup = TestHelper.TestSetup.Seeded;
            Guid robotId = Guid.NewGuid();

            object[] testData = new object[]
            {
                ("[spawn," + robotId + "]"),
                ("[west," + robotId + "]"),
                ("[north," + robotId + "]"),
                ("[east," + robotId + "]"),
                ("[east," + robotId + "]"),
                ("[east," + robotId + "]"),
            };

            foreach (string order in testData[..^1])
                Setup.ExplorationRobotControl.ExecuteOrder(Order.FromOrderString(order));

            Assert.AreEqual(20, Setup.ExplorationRobotControl.GetExplorationRobotCargo(robotId).AvailableCapacity().Amount);

            Assert.ThrowsException<ExplorationRobotControlException>(() => { Setup.ExplorationRobotControl.ExecuteOrder(Order.FromOrderString($"{testData[^1]}")); });
        }

        [TestMethod("MineMultipleTimes")]
        public void TestMineMultipleTimes()
        {
            using var Setup = TestHelper.TestSetup.Seeded;
            Guid robotId = Guid.NewGuid();

            object[] testData = new object[]
            {
                ("[spawn," + robotId + "]"),
                ("[west," + robotId + "]"),
                ("[north," + robotId + "]"),
                ("[mine," + robotId + "]"),
                ("[east," + robotId + "]"),
                ("[mine," + robotId + "]"),
            };

            foreach (string order in testData)
                Setup.ExplorationRobotControl.ExecuteOrder(Order.FromOrderString(order));

            Assert.AreEqual(0, Setup.ExplorationRobotControl.GetExplorationRobotCargo(robotId).AvailableCapacity().Amount);

            Guid system = Setup.ExplorationRobotControl.GetExplorationRobotPlanetarySystem(robotId).Value;
            Assert.AreEqual(2, Setup.ExplorationRobotControl.GetPlanetarySystemOreAmount(system).Amount);
        }

        [TestMethod("TwoExplorationRobotsOnSamePlanetarySystem")]
        public void TestTwoExplorationRobotsOnSamePlanetarySystem()
        {
            using var Setup = TestHelper.TestSetup.Seeded;
            Guid robot1 = Guid.NewGuid();
            Guid robot2 = Guid.NewGuid();

            object[] testData = new object[]
            {
                ("[spawn," + robot1 + "]"),
                ("[north," + robot1 + "]"),
                ("[west," + robot1 + "]"),
                ("[spawn," + robot2 + "]"),
                ("[west," + robot2 + "]"),
                ("[north," + robot2 + "]"),
            };

            foreach (string order in testData)
                Setup.ExplorationRobotControl.ExecuteOrder(Order.FromOrderString(order));

            Assert.AreEqual(20, Setup.ExplorationRobotControl.GetExplorationRobotCargo(robot1).AvailableCapacity().Amount);
            Assert.AreEqual(20, Setup.ExplorationRobotControl.GetExplorationRobotCargo(robot2).AvailableCapacity().Amount);

            Guid system1 = Setup.ExplorationRobotControl.GetExplorationRobotPlanetarySystem(robot1).Value;
            Guid system2 = Setup.ExplorationRobotControl.GetExplorationRobotPlanetarySystem(robot2).Value;

            Assert.AreEqual(system1, system2);

            ICollection<Guid> robots = Setup.ExplorationRobotControl.GetPlanetarySystemExplorationRobots(system2);
            Assert.AreEqual(2, robots.Count);
        }
    }
}
