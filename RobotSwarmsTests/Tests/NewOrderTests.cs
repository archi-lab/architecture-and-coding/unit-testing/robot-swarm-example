﻿using RobotSwarms.Domainprimitives;
using RobotSwarms.HICCUP;

namespace RobotSwarms.Tests
{
    [TestClass]
    [TestCategory("Other")]
    public class NewOrderTests
    {

        [TestMethod("OrderValidation")]
        public void TestOrderValidation()
        {
            Assert.ThrowsException<ExplorationRobotControlException>(() =>
            {
                Order.FromOrderString("[transport,]");
            }, "No appropriate validation exception thrown");
            Assert.ThrowsException<ExplorationRobotControlException>(() =>
            {
                Order.FromOrderString("[transport,1-1-1]");
            }, "No appropriate validation exception thrown");

            Assert.ThrowsException<ExplorationRobotControlException>(() =>
            {
                Order.FromOrderString("[explore,]");
            }, "No appropriate validation exception thrown");
            Assert.ThrowsException<ExplorationRobotControlException>(() =>
            {
                Order.FromOrderString("[explore,1-1-1]");
            }, "No appropriate validation exception thrown");

            Assert.ThrowsException<ExplorationRobotControlException>(() =>
            {
                Order.FromOrderString("[gohome,]");
            }, "No appropriate validation exception thrown");
            Assert.ThrowsException<ExplorationRobotControlException>(() =>
            {
                Order.FromOrderString("[gohome,1-1-1]");
            }, "No appropriate validation exception thrown");

        }


        [TestMethod("ExploreAndGoHome")]
        public void TestExploreAndGoHome()
        {
            using var Setup = TestHelper.TestSetup.Seeded;

            Guid explorationRobotId = Guid.NewGuid();
            Setup.ExplorationRobotControl.ExecuteOrder(Order.FromOrderString($"[spawn,{explorationRobotId}]"));

            Setup.ExplorationRobotControl.ExecuteOrder(Order.FromOrderString
                    ("[explore," + explorationRobotId.ToString() + "]"));

            Guid? planetarySystemId = Setup.ExplorationRobotControl.GetExplorationRobotPlanetarySystem(explorationRobotId);
            Assert.AreNotEqual("space station", Setup.ExplorationRobotControl.GetPlanetarySystemType(planetarySystemId));

            for (int i = 0; i < 4; i++)
            {
                Setup.ExplorationRobotControl.ExecuteOrder(Order.FromOrderString
                        ("[explore," + explorationRobotId.ToString() + "]"));
            }
            for (int i = 0; i < 5; i++)
            {
                try
                {
                    Setup.ExplorationRobotControl.ExecuteOrder(Order.FromOrderString
                            ("[gohome," + explorationRobotId.ToString() + "]"));
                }
                catch (ExplorationRobotControlException) { /*Do nothing*/ }
            }

            planetarySystemId = Setup.ExplorationRobotControl.GetExplorationRobotPlanetarySystem(explorationRobotId);
            Assert.AreEqual("space station", Setup.ExplorationRobotControl.GetPlanetarySystemType(planetarySystemId));
        }
    }
}
