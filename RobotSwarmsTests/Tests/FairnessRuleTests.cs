﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RobotSwarms.Domainprimitives;
using RobotSwarms.HICCUP;

namespace RobotSwarms.Tests
{
    [TestClass]
    [TestCategory("Other")]
    public class FairnessRuleTests
    {

        [TestMethod("4TwoExplorationRobotsMineOnSamePlanetarySystem")]
        public void Test4TwoExplorationRobotsMineOnSamePlanetarySystem()
        {
            using var Setup = TestHelper.TestSetup.Seeded;
            Guid robot1 = Guid.NewGuid();
            Guid robot2 = Guid.NewGuid();

            object[] testData = new object[]
            {
                ("[spawn," + robot1 + "]"),
                ("[west," + robot1 + "]"),
                ("[north," + robot1 + "]"),
                ("[east," + robot1 + "]"),
                ("[east," + robot1 + "]"),
                ("[spawn," + robot2 + "]"),
                ("[north," + robot2 + "]"),
                ("[mine," + robot2 + "]"),
                ("[west," + robot2 + "]"),
                ("[east," + robot2 + "]"),
                ("[east," + robot2 + "]"),
            };

            foreach (string order in testData)
                Setup.ExplorationRobotControl.ExecuteOrder(Order.FromOrderString(order));

            Assert.AreEqual(20, Setup.ExplorationRobotControl.GetExplorationRobotCargo(robot1).AvailableCapacity().Amount);
            Assert.AreEqual(5, Setup.ExplorationRobotControl.GetExplorationRobotCargo(robot2).AvailableCapacity().Amount);

            Assert.ThrowsException<ExplorationRobotControlException>(() =>
                Setup.ExplorationRobotControl.ExecuteOrder(Order.FromOrderString("[mine," + robot2 + "]")));

            Setup.ExplorationRobotControl.ExecuteOrder(Order.FromOrderString("[mine," + robot1 + "]"));
            Assert.AreEqual(0, Setup.ExplorationRobotControl.GetExplorationRobotCargo(robot1).AvailableCapacity().Amount);
        }
    }
}
