﻿namespace RobotSwarms.TestHelper.DTO
{
    public class ExplorationRobotDTO
    {

        public Guid? ExplorationRobotId { get; }
        public List<OrderDTO> Orders { get; }
        public Guid? PlanetarySystemId { get; set; }
        public string? Name { get; set; }

        public ExplorationRobotDTO(string? name, Guid? explorationRobotId, Guid? planetarySystemId, List<OrderDTO> orders)
        {
            Name = name;
            ExplorationRobotId = explorationRobotId;
            PlanetarySystemId = planetarySystemId;
            Orders = orders;
        }

        public ExplorationRobotDTO(string? name, Guid? explorationRobotId, Guid? planetarySystemId) : this()
        {
            Name = name;
            ExplorationRobotId = explorationRobotId;
            PlanetarySystemId = planetarySystemId;
        }

        public ExplorationRobotDTO()
        {
            Orders = new List<OrderDTO>();
        }
    }
}
