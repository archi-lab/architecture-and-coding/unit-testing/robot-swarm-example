﻿namespace RobotSwarms.TestHelper.DTO
{
    public class OrderDTO
    {
        public string? OrderString { get; set; }
        public Guid? ExplorationRobotId { get; set; }

        public OrderDTO(string? v, Guid? explorationRobotId)
        {
            this.OrderString = v;
            this.ExplorationRobotId = explorationRobotId;
        }

        public OrderDTO() { }
    }
}
