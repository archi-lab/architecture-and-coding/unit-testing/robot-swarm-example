﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RobotSwarms.TestHelper.Generator
{
    internal abstract class Generator<T>
    {
        protected readonly int Seed;

        private int rollCount = 1;

        protected Generator(int seed, int lastInt)
        {
            Seed = seed;
            LastInt = lastInt;
        }

        protected int RollModifier
        {
            get
            {
                int n = rollCount++;
                if (rollCount == int.MaxValue) rollCount = 1;
                return n;
            }
        }

        protected int LastInt { get; set; }

        protected static uint CurrentCPUTicks { get { return (uint)Environment.TickCount; } }

        protected int RollInt(int mod = int.MaxValue, int min = 0)
        {
            var r = mod - min;
            if (r < 0) { throw new Exception($"{nameof(min)} cannot be higher than {nameof(mod)}"); }
            var n = (CurrentCPUTicks * ((LastInt+Seed) * RollModifier) + Seed) % r;
            LastInt = (int)n + min;
            return LastInt;
        }

        public abstract T Next();


    }
}
