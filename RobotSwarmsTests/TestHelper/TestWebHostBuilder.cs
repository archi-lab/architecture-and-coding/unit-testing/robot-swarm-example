﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RobotSwarms.TestHelper
{
    public class TestWebHostBuilder
    {
        public void ConfigureServices(IServiceCollection services)
        {
            //services.AddDbContext<RobotSwarms.Shared.RobotSwarmsContext>();
            services.AddControllers();

            // App doesnt use MVC directly, however it needs to have access to the assembly to find and host the controller endpoints
            services.AddMvc()
                .AddApplicationPart(typeof(HICCUP.IExplorationRobotControl).Assembly);
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseDeveloperExceptionPage();
            app.UseHttpsRedirection();
            app.UseRouting();
            app.UseAuthorization();
            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }

        public static TestServer Server
        {
            get
            {
                var startup = new WebHostBuilder()
                .UseStartup<TestHelper.TestWebHostBuilder>();
                var server = new TestServer(startup) ?? throw new Exception("An unknown error occured.");
                return server;
            }
        }
    }
}
