﻿# Meilenstein AC1 "Roboter-Schwärme" - Domain Model und Entities für ein (etwas) größeres System

In diesem Meilenstein gehen wir in Richtung größerer Software-Systeme. Für eine gegebene 
Aufgabenstellung erstellen Sie ein Domain Model, identifizieren Entities und Value Objects, 
und implementieren das System gegen eine Anzahl von Unit Tests. Ziel ist es, diese Tests alle
"grün" zu haben, und dabei gut strukturierten Code produziert zu haben.

## Roboterschwärme in der Galaxis

Wir befinden uns im Jahr 2127. Das _**Hi**gh **C**Kommando **Kontrolle des **U**niversums und 
aller **P**laneten_ (HICCUP) ist eine Behörde der planetarischen Regierung, die für die 
Kontrolle der Aktionen von Explorationsrobotern (exploration robots) zuständig ist. Diese
Explorationsroboter bewegen sich über Planetensystem (planetary systems). Das Ziel ist es, 
dort Erze (ore) zu finden und abzubauen. Viele kleine und große Firmen unterhalten eigene 
Schwärme von solchen Robotern - auch deine eigene kleine Firma. 

HICCUP unterhält ein großes Sensor-Netzwerk, mit dem es neue Planetensysteme aufspürt und sie
auf Vorkommen von Erzen untersucht. Als allmächtige Super-Behörde möchte HICCUP aber die 
Kontrolle behalten, welche Roboter wo etwas abbauen. HICCUP beansprucht also, die Ausbeutung
der Planetensysteme selbst zu steuern - indem sie einzelne Roboter von bestimmten Firmen 
eben zu bestimmten Planeten schicken.

Bis jetzt haben das die Sachbearbeiter bei HICCUP alles noch durch Hyperraum-Funksprüche
gemacht, die sie jeweils an ihre bevorzugten Firmen abgesetzt haben. Das soll sich jetzt ändern: 
Um das System schneller und weniger anfällig gegen Mauschelei zu machen, wird diese Vergabe 
jetzt automatisiert. Das Architektur-Zielbild dabei ist folgendes: 

![Ziel-Architektur](./images/HICCUP2.png)


## Zwei Interfaces sind durch HICCUP vorgegeben

Jetzt müssen alle Firmen, die einen Roboterschwarm betreiben, eine Steuerungssoftware implementieren. 
Wie sie das machen, ist ihnen überlassen. Sie müssen nur die beiden Interfaces implementieren, die 
HICCUP vorgibt. 

* `IDetection` enthält Methoden, mit denen HICCUP dem Roboterschwarm mitteilen kann, dass sie
  einen neuen Planeten oder ein neues Erzvorkommen gefunden haben. Das ist erst einmal nur "zur Info", 
  und bedeutet noch keinen Auftrag, dorthin zu fahren.
* `IExplorationRobotControl` wird von HICCUP verwendet, um Aufträge an einzelne Roboter des Schwarms
   zu übermitteln. 	

Nachfolgend sind die Interfaces im Detail dargestellt.


### Interface `IDetection` 

Planetensysteme werden im Interface durch eine UUID (GUID oder "Globally Unique Identifier" im 
C#-Universum, Datentyp `Guid`) angesprochen. Der Einfachheit halber ist die Galaxy quadratisch, 
und die Roboter bewegen sich in Richtung Norden/Osten/Süden/Westen über die Planetensysteme. 
Auf einem Planetensystem können sich mehrere Roboter aufhalten. Das Erz, das die Roboter einsammeln 
können, wird (auch der Einfachheit halber) als ganzzahlige Einheit (Integer) verwaltet. 

![Nebula](./images/Grid.png)

Die Symbole in dieser Karte haben die folgende Bedeutung:

![](./images/no-titanium.png) - Planetensysteme, das kein Erz enthält

![](./images/titanium.png) - Planetensysteme mit etwas Erz (in diesem Fall mit der Menge "4")

![](./images/impassable.png) - Bereich mit dunkler Materie, den die Roboter nicht passieren können - sie müssen um ihn herum manövrieren.

![](./images/spaceShipYard.png) - Raumschiffswerft (space station), auf der neue Roboter gebaut werden

Das ist das Interface, das HICCUP vorgibt: 

```csharp
    public interface IDetect
    {
        /// <summary>
        /// HICCUP ruft diese Methode in deinem Softwaresystem auf, um dir über neu entdeckte Planetensysteme
        /// Bescheid zu sagen.
        /// </summary>
        /// <param name="planetarySystemId">Die ID des neu entdeckten Planetensystems</param>
        /// <param name="northNeighbourOrNull">
        /// ID des nördlichen Nachbarn, oder null, wenn es keinen nördlichen Nachbarn gibt, 
        /// sondern ein Gebiet mit dunkler Materie</param>
        /// <param name="eastNeighbourOrNull">siehe northNeighbourOrNull</param>
        /// <param name="southNeighbourOrNull">siehe northNeighbourOrNull</param>
        /// <param name="westNeighbourOrNull">siehe northNeighbourOrNull</param>
        public void NeighboursDetected(Guid? planetarySystemId,
            Guid? northNeighbourOrNull, Guid? eastNeighbourOrNull,
            Guid? southNeighbourOrNull, Guid? westNeighbourOrNull);


        /// <summary>
        /// HICCUP ruft diese Methode in deinem Softwaresystem auf, um dir über ein neu entdecktes 
        /// Erzvorkommen Bescheid zu sagen.
        /// </summary>
        /// <param name="planetarySystemId">Die ID Planetensystems, um das es geht</param>
        /// <param name="ore">
        /// Die Menge an entdecktem Erz (kann auch 0 sein, im Fall einer Korrektur, aber nicht <0)
        /// </param>
        void OreDetected(Guid? planetarySystemId, Ore ore);
    }
```

Deine Software muss sich auf der Basis dieser Informationen eine eigene Karte der Galaxie bauen. Die 
Grid-Struktur macht das grundsätzlich einfach, aber man muss trotzdem auf einige Dinge aufpassen - 
hier ein kleines Beispiel. Der Einfachheit halber verwenden wir in diesem kleinen Beispiel Int-IDs für 
Planetensysteme anstelle von UUIDs. Nehmen wir an, dass das Planetensystem mit der Raumschiffswerft 
die ID 0 hat. Dann ruft HICCUP die deine API-Methode `NeighboursDetected( 0, 1, 2, null, 4 )` 
auf. Deine Karte muss danach so aussehen: 

![Galaxie nach erstem Aufruf](./images/detect1.jpg)

Dann erfolgt ein zweiter Aufruf von HICCUP, dieses Mal `NeighboursDetected( 1, 5, null, 0, 8 )`. 
Die Galaxie-Karte sieht dann wie folgt aus:

![Galaxie nach zweitem Aufruf](./images/detect2.jpg)

Tatsächlich gibt es aber auch eine Verbindung zwischen den Planetensystemen 4 und 8! D.h. wenn man bei 0 
beginnt und sich erst nach Norden, dann nach Westen bewegt, erreicht man dasselbe Planetensystem wie bei einer 
Bewegung nach West-Nord (Planetensystem 8). Deine Karte muss also auch die Verbindung zwischen 4 und 8 
"ableiten", die gelb markiert ist.

![Wahre Galaxie nach zweitem Aufruf](images/detect3.jpg)


### Interface `IExplorationRobotControl` 

Der HICCUP-Server ruft dieses Interface in deiner Software auf, um Roboter bauen zu lassen, sie zu bewegen, und um 
Erz zu sammeln. Zu diesem Zweck übermittelt HICCUP Kommandos im String-Format, wie nachfolgend aufgelistet: 

* `[build,<explorationBotUUID>]` - Baue einen neuen Roboter in der Raumwerft, und weise ihm die ID `explorationBotUUID` zu
  * kann nicht ausgeführt werden, wenn die ID bereits einem anderen Explorationsbot zugewiesen wurde
* `[collect,<explorationBotUUID>]` - Sammle das (komplette) Erz auf dem Planetensystem ein, auf dem sich der 
   Roboter gerade befindet
  * kann nicht ausgeführt werden, wenn das Planetensystem kein Erz enthält
* `[north,<explorationBotUUID>]` - Bewege den Roboter zum nördlich gelegenen Nachbar-Planetensystem
  * kann nicht ausgeführt werden, wenn nördlich des Planetensystems ein Gebiet mit dunkler Materie liegt, 
    oder noch kein nördlicher Nachbar bekannt ist
* `[east,<explorationBotUUID>]` - Siehe "north"
* `[south,<explorationBotUUID>]` - Siehe "north"
* `[west,<explorationBotUUID>]` - Siehe "north"

Eine gültige Befehlsfolge wäre demnach:

* `[build,70343c17-11d1-46bb-8159-7f4c6fc17d40]`
* `[south,70343c17-11d1-46bb-8159-7f4c6fc17d40]`
* `[build,b33e3e5c-b985-409b-89ef-ca037bc2caa6]`
* `[east,70343c17-11d1-46bb-8159-7f4c6fc17d40]`
* `[collect,70343c17-11d1-46bb-8159-7f4c6fc17d40]`
* `[north,b33e3e5c-b985-409b-89ef-ca037bc2caa6]`
* `[collect,b33e3e5c-b985-409b-89ef-ca037bc2caa6]`

Das würde bedeuten, dass zwei Erkundungsroboter gebaut werden. Der erste geht ein Planetensystems nach 
Süden und dann eins nach Osten. Der andere Erkundungsroboter geht nach Norden. Letztendlich haben dann beide 
Roboter Erz gesammelt. 

Das Interface sieht folgendermaßen aus:

```csharp
    public interface IExplorationRobotControl
    {
        /// <summary>
        /// Führt den gegebenen Befehl an einen Roboter aus.
        /// </summary>
        /// <param name="orderString">Befehl an den Roboter (Format siehe README.md)</param>
        /// <exception cref="ExplorationRobotControlException">Falls der Befehl ungültig ist oder nicht ausgeführt werden kann</exception>

        public void ExecuteOrder(String orderString);


        /// <summary>
        /// Gibt zurück, wie viel Erz der Roboter derzeit als Fracht transportiert.
        /// </summary>
        /// <param name="explorationRobotId">Erkundungsroboter, für den die Ladung ermittelt werden soll</param>
        /// <returns>Aktuelle Menge der Erzladung im Explorationsroboter</returns>
        /// <exception cref="ExplorationRobotControlException">Wenn explorationRobotId keine gültige ID ist</exception>
        public int GetExplorationRobotCargo(Guid? explorationRobotId);


        /// <summary>
        /// Gibt zurück, auf welchem Planeten der Roboter gerade steht.
        /// </summary>
        /// <param name="explorationRobotId">ID des Roboters</param>
        /// <returns>ID des Planetensystems, auf dem sich der Erkundungsroboter gerade befindet</returns>
        /// <exception cref="ExplorationRobotControlException">Wenn explorationRobotId keine gültige ID ist</exception>
        public Guid? GetExplorationRobotPlanetarySystem(Guid? explorationRobotId);


        /// <summary>
        /// Gibt den Typ eines Planetensystems zurück.
        /// </summary>
        /// <param name="plantarySystemId">Id des Planetensystems</param>
        /// <returns>
        /// Entweder "space shipyard", "regular visited", oder "regular not visited".
        /// - "space shipyard" heißt: das ist die Raumwerft.
        /// - "regular visited" bedeutet: Das ist nicht die Raumwerft, aber ich kenne das Planetensystem 
        ///   (d.h. seine ID wurde mir von HICCUP schon übermittelt), und mindestens einer meiner Roboter hat
        ///   das Planetensystem schon besucht.
        /// - "regular not visited" heißt: Das ist nicht die Raumwerft, aber ich kenne das Planetensystem 
        ///   (d.h. seine ID wurde mir von HICCUP schon übermittelt), aber keiner meiner Roboter war bis jetzt dort.
        /// </returns>
        /// <exception cref="ExplorationRobotControlException">
        /// Falls die plantarySystemId unbekannt ist, d.h. diese ID wurde von HICCUP noch nicht übermittelt.
        /// </exception>
        public string GetPlanetarySystemType(Guid? plantarySystemId);


        /// <summary>
        /// Gibt die Menge Erz auf einem Planetensystem zurück, soweit bekannt.
        /// </summary>
        /// <param name="planetarySystemId">Id des Planetensystems</param>
        /// <returns>
        /// Die Menge an Titan, die noch auf dem Asteroiden vorhanden ist (kann 0 sein, aber nicht Null)
        /// </returns>
        /// <exception cref="ExplorationRobotControlException">
        /// Falls die plantarySystemId unbekannt ist, d.h. diese ID wurde von HICCUP noch nicht übermittelt.
        /// </exception>
        public int GetPlanetarySystemOreAmount(Guid? planetarySystemId);


        /// <summary>
        /// Gibt die Roboter auf einem Planetensystem zurück.
        /// </summary>
        /// <param name="planetarySystemId"></param>
        /// <returns>
        /// Liste der Roboter-IDs, die sich derzeit auf diesem Planetensystem befinden (Liste kann leer 
        /// sein, aber nicht null)
        /// </returns>
        /// <exception cref="ExplorationRobotControlException">
        /// Falls die plantarySystemId unbekannt ist, d.h. diese ID wurde von HICCUP noch nicht übermittelt.
        /// </exception>
        List<Guid> GetPlanetarySystemExplorationRobots(Guid? planetarySystemId);


        /// <summary>
        /// Gibt alle bekannten Planetensysteme zurück.
        /// </summary>
        /// <returns>
        /// Liste aller Planetensysteme, die von HICCUP schon übermittelt wurden (Liste kann leer sein, aber nicht null)
        /// </returns>
        List<Guid> GetKnownPlanetarySystems();


        /// <summary>
        /// Als "Notfallmethode" für das Softwaresystem: setze alles auf den Ausgangszustand zurück. 
        /// Lösche alle Robots und die Galaxiekarte (also alle Planentensysteme). Nur die Raumwerft bleibt übrig, 
        /// mit 0 Erz.
        /// </summary>
        /// <returns>
        /// Die ID des einen übriggebliebenen Planetensystems, auf dem die Raumwerft steht.
        /// </returns>
        public Guid ResetSystem();    }
```



## Die Tests

Es gibt eine Anzahl von Tests, die die Implementation des APIs (also der beiden gegebenen Interfaces) testen. 
Diese Tests spielen sozusagen die Rolle des HICCUP-Servers, übermitteln Informationen und Befehle und vergleichen 
dann das Resultat gegen das erwartete Ergebnis. 


## Ein Beispiel-Entity: `Beacon`

Es gibt ein vorimplementiertes Entity: `Beacon`. Dieses Entity dient als Beispiel, um einen Schnellstart 
in C# ORM zu bieten. Die Beacons sind dazu in der Lage, ein Planetensystem anhand eines Namens (anstatt 
der Guid des Systems) zu finden. Darüber hinaus können Beacons auch auf andere, umliegende Systeme 
verweisen. Dies ist jedoch nicht notwendigerweise der Fall. Da die Beacons nicht Teil der HICCUP-Interfaces
sind, gibt es für sie auch keine Tests oder Vorgaben. Man kann die entsprechenden Klassen so erweitern 
oder verändern, wie man möchte - oder man nutzt sie einfach als Beispiel zum "Spicken".



# Die Aufgabe im Meilenstein AC1

In diesem ersten Meilenstein geht es darum, sich dieser Aufgabenstellung zu nähern und so viel davon umzusetzen, 
wie in der verfügbaren Zeit möglich ist. 


## 1) Erstelle ein Domain Model

Am besten tust du dich mit Kolleg*innen in einer kleinen Gruppe zusammen, und erstellt ein Domain Model. 
Nutzt ein Tool wie z.B. Miro - auf Wunsch erstelle ich euch ein eigenes Board, einfach via Teams Bescheid 
sagen. Ihr könnt die Form eines einfachen UML-Klassendiagramms wählen, oder einfach eine informelle Notation
mit Pfeilen und Kästchen. Wichtig ist das folgende: 

- Lasst die Gedanken an technische Umsetzung erst einmal außen vor. Welche Geschäftsobjekte kommen in der 
  Aufgabenstellung vor? Die folgenden beiden Videos können dabei helfen (man kann sie auch im "Schnelldurchlauf"
  anschauen):
  - [Textanalyse zur Erstellung eines fachlichen Glossars](https://www.youtube.com/watch?v=Jyp0FQIMovY): 
    Das fachliche Glossar ist eine Vorstufe des Domain Models, bei dem erst einmal alle Geschäftsobjekte 
    gesammelt werden. Das Video erklärt, wie man die relevanten Geschäftsobjekte durch einfache Analyse
    des Textes - sozusagen mit Papier und Textmarker - findet.
  - [Erstellung des fachlichen Datenmodells](https://www.youtube.com/watch?v=-xlb9olccHI): In meinen Videos unterscheide
    ich beim Domain Model noch einmal zwischen dem "fachlichen Datenmodell", das noch keine Beziehungsrichtung
    vorsieht und noch etwas weiter vom Code weg ist, und dem "logischen Datenmodell" (siehe nächstes Video). 
    Du kannst beide Schritte auch zusammenfassen. Die Videos versuchen die Schritte "sauber aufzudröseln"
    und trennen deshalb beide Schritte.
  - [Erstellung des logischen Datenmodells](https://www.youtube.com/watch?v=wSXWknKQkbU): Hier wird der Schritt
    vom fachlichen zum logischen (Code-nahen) Datenmodell beschrieben, insbesondere mit dem Weglassen / Hinzufügen
    von Geschäftsobjekten und dem Übergang von ungerichteten zu gerichteten Assoziationen.
  - [Warum unidirektionale Beziehungen?](https://www.youtube.com/watch?v=RufBXPTt1SA): Eigentlich wären 
    bidirektionale Beziehungen ja praktisch, oder? Sie binden aber Klassen fest aneinander und erschweren 
    Wartung und Wiederverwendung des Codes. Dieses Video motiviert das.


## 2) Identifiziere Entities und Value Objects

Entscheidet in eurer Kleingruppe, welche Geschäftsobjekte Entities und welche Value Objects sind. 
[Das Erklärungsvideo zu Entities und Value Objects](https://www.youtube.com/watch?v=pYg7I3UJ3Ls) hilft bei
der Einordnung. 


## 3) Umsetzung in Code nach Code-First-Ansatz, mit Persistierung

Das ist jetzt mit Abstand der größte Teil der Aufgabe: Setze das Domain Model in Code um, und zwar 
mit Hilfe des EF Core 6 Frameworks. Wir werden uns damit im Workshop am 25.10. beschäftigen. Wichtig dabei
ist: 
- Wähle bitte den **Code-First-Ansatz**. (Code-First heißt: Die Struktur der Datenbank wird über 
  den Sourcecode des Entities festgelegt.) Code-First ist immer dann besser, wenn wir weder eine 
  bestehende Datenbank noch besondere Optimierungs-Anforderungen auf Datenbank-Ebene haben. Der
  Ansatz hat den großen Vorteil, näher am fachlichen Denken und dem Domain Model zu sein.
- Deine Entities müssen persistiert werden können. Du musst also Repositories (im DDD-Sinne)
  schreiben. Damit werden bestimmte "naheliegende" Lösungen (Stichwort 2-dimensionale Arrays)
  erschwert.
- Auch wenn du das Domain Model in der Kleingruppe zusammen mit anderen entwickelt hast, schreibe
  bitte unbedingt **selbst** deinen eigenen Code. Du kannst deine Lösung aber gern mit den anderen
  vergleichen. 

Schau mal, wie weit du mit der Lösung kommst. Ziel ist es, am Ende alle Tests grün zu haben und dabei 
guten Code produziert zu haben. Wir werden aber diese Aufgabenstellung für alle Meilensteine
im Hauptteil des SoftwareLabs benutzen und behutsam weiterentwickeln. Wenn du in also diesem Meilenstein
nicht fertig wirst, ist das nicht schlimm - es ist noch Zeit bis Ende des Jahres.

# Meilenstein AC2 - Domain Primitives und ein weiteres Feature

Das Zertifizierungsprogramm des _**Hi**gh **C**kommandos **C**ontrol of the **U**niverse and all **P**lanets_
(HICCUP) geht nun in seine n�chste Runde. Um die Qualit�t des Codes weiter zu verbessern, verlangt die Zertifizierung nun
die Nutzung von bestimmten _Domain Primitives_ als Basisklassen. 


## Zwei neue Kommandos

Es gibt zwei neue, zus�tzliche Kommandos: 

* `[explore,<robot-guid>]` - der Roboter w�hlt einen zuf�lligen Planeten, der noch nicht besucht wurde, 
  und bewegt sich dorthin.
	- Wenn alle Planeten in der Umgebung des Roboters bereits besucht wurden, kann der Roboter zu einem 
      zuf�lligen bekannten Planeten gehen.
	- Wenn es keinen umgebenden Planeten gibt, kann der Befehl nicht ausgef�hrt werden, und eine 
      `ExplorationBotControlException` wird ausgel�st.
* `[gohome,<robot-guid>]` - der Roboter bewegt sich einen Schritt auf die Spacestation (wo er gebaut wurde) zu.
    - Bei `gohome` ist es nicht erforderlich, den k�rzesten Weg zur Spacestation zu w�hlen. Man k�nnte also einfach 
      die Bewegungen des Roboters aufzeichnen und sie dann zur�ckverfolgen.
    - Der Befehl kann nicht ausgef�hrt werden, wenn sich der Roboter bereits auf der Spacestation befindet. 
      In diesem Fall wird eine `ExplorationBotControlException` geworfen.


## Domain Primitives

Bisher wurden bestimmte Objekte nur als Basisklassen kodiert, z. B. Strings. HICCUP m�chte ihnen nun etwas Gesch�ftslogik
hinzuf�gen. Um das zu vereinheitlichen, hat HICCUP bereits die folgenden Domain Primitives vorgegeben, die 
von jeder zertifizierten Software implementiert werden sollen.
 
1. Command
   * Dieses Domain Primitive stellt einen Auftrag an einen Erkundungsbot dar.
   * Kommandos brauchen eine Factory-Methode zur Erzeugung aus einem String, z.B.
     `[north,f56177e3-8a79-4514-841c-2e2c837f467c]`
   * Ung�ltige Zeichenfolgen m�ssen erkannt werden, z.B. `[tr,1-1-1-1-1]` or `[bl�mpf,f56177e3-8a79-4514-841c-2e2c837f467c]` 
2. Ore
    * Dieses Domain Primitive repr�sentiert eine Menge Erz (Ore) auf einem Planeten, oder in einem Exploration Bot
    * Es kann leer sein, d.h. es kann eine "0" Menge geben.
    * Neben einer validierenden Factory-Methode sind Methoden zum Addieren und Subtrahieren von Ore erforderlich.
    * Au�erdem m�ssen zwei Mengen `Ore` vergleichbar sein.
3. Cargo
    * Dies ist ein Domain Primitive, um eine Fracht zu darzustellen, die ein Roboter transportiert.
    * Es gibt eine validierende Factory-Methode. 
    * Weiter werden Methoden ben�tigt, um zu entscheiden, wie viel `Ore` in den verf�gbaren `Cargo`-Space gef�llt werden 
      kann, und was danach �brig bleibt.
4. Direction
    * Dieses Domain Primitive repr�sentiert eine Himmelsrichtung (als Enum).
    * Es ben�tigt eine Factory-Methode aus Strings mit Validierung. 
    * Au�erdem sollte es eine Methode zur Berechnung einer entgegengesetzten Himmelsrichtung haben.
5. DirectionPath
    * Diese Klasse beschreibt den Weg, den ein Erkundungsbot nimmt, wenn er die Spacestation verl�sst. Sie kann 
      verwendet werden, um seine Schritte zur�ckzuverfolgen, wenn der Befehl `[gohome,...]` gegeben wird.
    * Im Grunde funktioniert es wie ein Stapel, in den Bewegungen (Kardinalrichtungen) gelegt werden.
    * Neben einer Factory-Methode werden Methoden ben�tigt, um eine Bewegung hinzuzuf�gen, den n�chsten 
      "gohome"-Schritt anzugeben, und ein Backtracking (Erzeugen eines neuen Pfades, wobei die letzte Bewegung 
      ersetzt wird)


## Fairness-Regeln beim Mining

Als Reaktion auf K�mpfe zwischen Robotern gibt HICCUP eine Fairness-Regel aus, wenn sich mehr als ein Erkundungsbot auf einem Planeten befindet: 
	
1. Der leerste Roboter darf zuerst sammeln.
2. Er kann so viel Erz mitnehmen, wie seine Ladekapazit�t und die verf�gbare Menge auf dem Asteroiden zulassen.
3. Befindet sich danach noch Erz auf dem Planeten, ist der "n�chstleere" Roboter an der Reihe, und so weiter.
4. Wenn der falsche Roboter versucht zu sammeln, muss eine ExplorationBotControlException (oder eine Unterklasse) geworfen
   werden.

Hinweis: diese Regel kann eine zyklische Abh�ngigkeit zwischen Packages verursachen. Euer Code m�ssen sollte versuchen, 
das zu vermeiden. Daf�r kann man eines der SOLID-Prinzipien nutzen.


## Anwendung von Clean Code, SOLID und Domain Primitives

Analysiert euren Code auf die Einhaltung der Clean-Code-Regeln und SOLID-Prinzipien. Refactored wo n�tig. Verwendet Domain Primitives (siehe Dienstags-Workshop am 20.12.), um Basisfunktionalit�t zu kapseln.


# Meilenstein AC3 - Implementation eines REST-APIs

Das _**Hi**gh **C**kommandos **C**ontrol of the **U**niverse and all **P**lanets_ (HICCUP) m�chte seine Steuerung
auf ein REST-API umstellen. Dieses wird zun�chst einmal f�r `ExplorationRobots` umgesetzt. Konkret soll der
Bau von Robotern, �nderungen an Robotern und Abfragen zum Roboter-Schwarm �ber das REST-API m�glich sein. 
Dar�ber hinaus soll es erm�glicht werden, dem Roboter �ber REST Befehle zu geben.

Das sind die REST-Endpoints, die umgesetzt werden sollen:


|Requirement # | URI | VERB |
|---|---|---|
| 1. Alle Roboter ausgeben                                      | /api/explorationRobots                                | GET |
| 2. Neuen Roboter anlegen                                      | /api/explorationRobots                                | POST |
| 3. Einen bestimmten Roboter zur�ckgeben, anhand seiner ID     | /api/explorationRobots/{explorationRobot-id}          | GET |                                           
| 4. Einen bestimmten Roboter zerst�ren                         | /api/explorationRobots/{explorationRobot-id}          | DELETE |
| 5. Name eines Robots �ndern                                   | /api/explorationRobots/{explorationRobot-id}          | PATCH |
| 6. Kommando an einen Robot schicken                           | /api/explorationRobots/{explorationRobot-id}/orders   | POST |
| 7. Alle bisherigen Kommandos eines Robots anfragen            | /api/explorationRobots/{explorationRobot-id}/orders   | GET |
| 8. Kommando-Historie eines Robots l�schen                     | /api/explorationRobots/{explorationRobot-id}/orders   | DELETE | 


## Hinweise

- Wir haben die alten Tests aus Kompatibil�tsgr�nden drin gelassen, aber um neue Tests f�r die REST-APIs erg�nzt.
- DTOs for `Order` und f�r `ExplorationRobot` sind der Einfachheit halber schon angelegt, im Package 
  `ExplorationRobot\Application`.
  - In `OrderDTO` ist der Einfachheit halber der die Guid des Robots neben dem Command-String noch einmal
    explizit als Property drin - streng genommen br�uchte man das nicht, weil die Guid aus dem Command-String
    zu entnehmen ist. Aber so wird die Abbildung auf das bisherige API einfacher.

